#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-prometheus

clear_app() {
    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    supervisorctl update
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app