#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

configure_grafana()
{
    bash -x grafana_api_set.sh
    if [ $? -ne 0 ]
    then
        echo "grafana configure fail, recover the grafana config file now"
        bash -x grafana_recover.sh
        exit 1
    fi
}

init_app() {
    systemctl daemon-reload
    systemctl enable grafana-server
    configure_grafana
}

init_app

# Start
bash -x $BaseDir/start.sh