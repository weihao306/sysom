#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

ensure_mysql_active() {
    # 1. First ensure mysql installed
    rpm -q --quiet mariadb-server || yum install -y mariadb-server

    # 2. Then ensure mariadb.service is active
    result=$(systemctl is-active mariadb.service)
    if [ "$result" != "active" ]; then
        systemctl enable mariadb.service
        systemctl start mariadb.service
    fi
}

setup_database() {
    mysql -uroot -e "create user if not exists 'sysom'@'%' identified by 'sysom_admin';"
    mysql -uroot -e "grant usage on *.* to 'sysom'@'localhost' identified by 'sysom_admin'"
    mysql -uroot -e "create database sysom character set utf8;"
    mysql -uroot -e "create database grafana character set utf8;"
    mysql -uroot -e "grant all privileges on sysom.* to 'sysom'@'%';"
    mysql -uroot -e "grant all privileges on grafana.* to 'sysom'@'%';"
    mysql -uroot -e "flush privileges;"
}

init_app() {
    ensure_mysql_active
    setup_database
}

init_app

# Start
bash -x $BaseDir/start.sh