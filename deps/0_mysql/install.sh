#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

install_app() {
    rpm -q --quiet mariadb-server || yum install -y mariadb-server
    systemctl enable mariadb.service
}

install_app
