-- 获取当前时间（以秒为单位）
local time = redis.call('TIME')
local current_time = tonumber(time[1]) + tonumber(time[2]) / 1000000

-- 删除过期条目
local expire_times = redis.call("HGETALL", KEYS[2])
for i = 1, #expire_times, 2 do
    local field = expire_times[i]
    local expire_time = tonumber(expire_times[i + 1])
    if (expire_time >= 0 and expire_time < current_time) then
        -- 删除过期的成员
        redis.call("HDEL", KEYS[1], field)
        redis.call("HDEL", KEYS[2], field)
    end
end

return redis.call("HLEN", KEYS[1])