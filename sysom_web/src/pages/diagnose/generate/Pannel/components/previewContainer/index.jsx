import styles from './index.less'
import SectionContainer from "@/pages/diagnose/generate/Pannel/components/sectionContainer";
import { componentConfigs } from '../../component-config';

/**
 * Preview Container
 * @returns 
 */
function PreviewContainer({ isDragging }) {
    return (
        <div className={styles.containerWrap}>
            <SectionContainer isDragging={isDragging} type='taskform' acceptItem={Object.keys(componentConfigs).map((key) => componentConfigs[key]).filter((item, index) => item.container === 'taskform').map((value, index) => { return value.type })} />
            <SectionContainer isDragging={isDragging} type='panels' acceptItem={Object.keys(componentConfigs).map((key) => componentConfigs[key]).filter((item, index) => item.container === 'panels').map((value, index) => { return value.type })} />
        </div>
    );
}

export default PreviewContainer;
