import { Form, Input, InputNumber, Select } from "antd";
import { componentConfigs } from "@/pages/diagnose/generate/Pannel/component-config";
import { templateReplace } from "@/pages/diagnose/generate/Pannel/util";
import { getLocale } from "umi";
import { usePanelGeneratorStore } from "@/pages/diagnose/generate/Pannel/store/panelGeneratorStore";
import { useMockStore } from "@/pages/diagnose/generate/Pannel/store/mockData";
import { useGlobalVariablesStore } from "@/pages/diagnose/generate/Pannel/store/glovalVariablesStore";

/**
 * preview component render
 * @param type component type
 * @param value component config
 * @returns {JSX.Element} component
 * @constructor CreateComponent
 */
export const CreateComponent = ({ type, value }) => {
    const mockStore = useMockStore((state) => state.data)
    const globalVariableStore = useGlobalVariablesStore((state) => state.data)
    const isDraggingNow = usePanelGeneratorStore((state) => state.isDraggingNow)
    const lang = getLocale()
    const newPanelConfig = { ...componentConfigs[type] }
    const InstComponent = newPanelConfig.component
    const datas = mockStore.datas
    // use if & switch according to the previous work in sysom_web/src/pages/diagnose/component/taskform
    if (newPanelConfig.container === 'taskform') {
        switch (newPanelConfig.config.type) {
            case 'text':
                return (
                    <Form.Item name={value.name} label={value.label} tooltip={value.tooltip}
                        initialValue={value.initialValue}
                        style={{ margin: 0 }}>
                        <Input value={value.initialValue}></Input>
                    </Form.Item>
                )
            case 'digit':
                return (
                    <Form.Item name={value.name} label={value.label} tooltip={value.tooltip}
                        initialValue={value.initialValue}
                        style={{ margin: 0 }}>
                        <InputNumber style={{ width: '100%' }} defaultValue={value.initialValue}></InputNumber>
                    </Form.Item>
                )
            case 'select':
                return (
                    <Form.Item name={value.name} label={value.label} tooltip={value.tooltip}
                        initialValue={value.initialValue}
                        style={{ margin: 0 }}>
                        <Select options={value.options} defaultValue={value.initialValue}
                            style={{ minWidth: '100px', width: '100%' }}></Select>
                    </Form.Item>
                )
        }
    } else {
        let panelDataIndex
        if (newPanelConfig.config.datasource) {
            panelDataIndex = templateReplace(newPanelConfig.config.datasource, globalVariableStore)
        }
        newPanelConfig.config.title = templateReplace(newPanelConfig.config.title, globalVariableStore)

        const data = datas[panelDataIndex]?.data
        if (type === 'row') {
            return (
                <InstComponent key={newPanelConfig.name + Date.now().toString().substring(-5)}
                    configs={value}
                    isDragging={isDraggingNow}
                    style={{ marginTop: 0 }}
                />
            )
        } else {
            return (
                <InstComponent key={newPanelConfig.name + Date.now().toString().substring(-5)}
                    configs={value}
                    data={data}
                    datas={datas}
                    globalVariables={globalVariableStore}
                    style={value.style}
                />
            )
        }
    }
}
