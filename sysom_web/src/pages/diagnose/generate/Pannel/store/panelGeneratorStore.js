import { create } from "zustand";

export const usePanelGeneratorStore = create((set) => ({
    data: {
        servicename: 'test',
        taskform: [],
        pannels: [],
        version: 1
    },
    isDraggingNow: false,
    setIsDraggingNow: (isDraggingNow) => set({ isDraggingNow }),
    setConfigStore: (data) => {
        typeof data === 'function' ? set((prev) => ({ data: data(prev.data) })) : set({ data })
    },
}))