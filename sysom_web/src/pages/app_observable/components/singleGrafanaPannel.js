import GrafanaWrap from '../../Monitor/grafana'

/**
 * 单面板配置
{
    "menuName": "menu.app_observable.ntopo",
    "type": "multiGrafanaPannel",
    "config": {
        "pannelId": "ntopo",
        "pannelName": "",
        "pannelUrl": "/grafana/d/H04tHN34k/ntopo"
    },
    "locales": {
        "zh-CN": {
            "menu.app_observable.ntopo": "网络拓扑"
        },
        "en-US": {
            "menu.app_observable.ntopo": "Network Topology"
        }
    }
}
 * @param {*} props 
 */
const SingleGrafanaPannel = (props) => {
    const queryParams = !!props.queryParams ? props.queryParams : {};
    // queryParams to query string
    const queryStr = Object.keys(queryParams).map(key => key + '=' + queryParams[key]).join('&').trim();
    let targetUrl = props.config.pannelUrl;
    if (queryStr.length > 0) {
        targetUrl += `?${queryStr}`
    }
    return (
        <div>
            <GrafanaWrap src={targetUrl} />
        </div>
    )
};

export default SingleGrafanaPannel;