import { useState, useRef } from 'react';
import { Button, Col, Row } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { ModalForm, ProFormText, ProFormSelect, ProFormSwitch, ProFormDateTimePicker, ProForm, ProFormTextArea } from "@ant-design/pro-form";
import { useIntl, FormattedMessage } from 'umi';
import { submitReleasedHotfixInfo } from '../service'


const CreateReleasedHotFixForm = (props) => {
    const intl = useIntl();
    const hotfixReleasedFormRef = useRef();
    const [isScrap, setIsScrap] = useState(true);
    const deprecatedSwitchHandler = (e) => { setIsScrap(!isScrap) }
    return (
        <ModalForm
            formRef={hotfixReleasedFormRef}
            title={intl.formatMessage({
                id: 'pages.hotfix.create_hotfix_config',
                defaultMessage: 'Batch import',
            })}
            trigger={
                <Button type="primary">
                    <PlusOutlined />
                    <FormattedMessage id="pages.hotfix.create_hotfix" defaultMessage="create hotfix config" />
                </Button>
            }
            onFinish={async (value) => {
                const result = await submitReleasedHotfixInfo(value)
                if (result.code === 200) {
                    hotfixReleasedFormRef.current?.resetFields()
                    props.addHotfixReleasedSuccess()
                } else { }
                return true
            }}
        >
            <Row>
                <Col span={12}>
                    <ProFormText
                        width="md"
                        name="hotfix_id"
                        label={intl.formatMessage({ id: 'pages.hotfix.hotfixid', defaultMessage: 'Hotfix ID' })}
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.hotfixid',
                            defaultMessage: 'The Hotfix ID'
                        })}
                    />
                </Col>
                <Col span={12}>
                    <ProFormText
                        name="released_kernel_version"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_kernelverison',
                            defaultMessage: 'pages.hotfix.tooltips.released_kernelverison'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_kernel_version',
                            defaultMessage: 'Released kernel version'
                        })}
                    />
                </Col>


            </Row>

            <Row justify="space-around">
                <Col span={12}>
                    <ProFormSelect
                        name="serious"
                        valueEnum={{
                            0: "可选安装",
                            1: "建议安装",
                            2: "需要安装"
                        }}
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.serious_level',
                            defaultMessage: 'The suggestion level of installing this hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.serious',
                            defaultMessage: 'Serious Level'
                        })}
                    />
                </Col>

                <Col span={12}>
                    <ProFormSelect
                        name="fix_system"
                        width="md"
                        options={[
                            {
                                label: "调度",
                                value: 0,
                            },
                            {
                                label: "内存",
                                value: 1,
                            },
                            {
                                label: "网络",
                                value: 2,
                            },
                            {
                                label: "存储",
                                value: 3,
                            },
                            {
                                label: "其他",
                                value: 4,
                            }
                        ]}
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.fix_system',
                            defaultMessage: 'The Sub system this hotfix fixed',
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.fix_system',
                            defaultMessage: 'Fixed Subsystem'
                        })}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <ProFormTextArea
                        name="serious_explain"
                        label={intl.formatMessage({
                            id: 'pages.hotfix.serious_explain',
                            defaultMessage: 'Serious Explain'
                        })}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <ProFormText
                        name="download_link"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.downloadlink',
                            defaultMessage: 'Please input the download link of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.download_link',
                            defaultMessage: 'Download Link'
                        })}
                        rules={[
                            {
                                pattern: /^https?:\/\/\S+/,
                                message: (
                                    <FormattedMessage
                                        id="pages.hotfix.download_link_error"
                                        defaultMessage="download_link_error"
                                    />
                                ),
                            }
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <ProFormDateTimePicker
                        name="released_time"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_time',
                            defaultMessage: 'Please input the released time of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_time',
                            defaultMessage: 'Released Time'
                        })}
                    />
                </Col>
            </Row>

            <Row>
                <Col span={24}>
                    <ProFormTextArea
                        name="description"
                        colProps={{ span: 24 }}
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.description',
                            defaultMessage: 'The description of this hotfix.'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.description',
                            defaultMessage: 'Description'
                        })}
                    />
                </Col>
            </Row>

            <Row>
                <Col span={4}>
                    <ProFormSwitch
                        name="deprecated"
                        label={intl.formatMessage({
                            id: 'pages.hotfix.deprecated',
                            defaultMessage: 'Deprecated'
                        })}
                        checkedChildren={intl.formatMessage({
                            id: 'pages.hotfix.yes',
                            defaultMessage: 'Yes'
                        })}
                        unCheckedChildren={intl.formatMessage({
                            id: 'pages.hotfix.no',
                            defaultMessage: 'No'
                        })}
                        onChange={deprecatedSwitchHandler}
                    />
                </Col>
                <Col span={20}>
                    <ProFormText
                        name="deprecated_info"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.deprecated_info',
                            defaultMessage: 'Please input the deprecated infomation of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.deprecated_info',
                            defaultMessage: 'Deprecated Info'
                        })}
                        disabled={isScrap}
                    />
                </Col>
            </Row>

        </ModalForm>
    )
}

export default CreateReleasedHotFixForm;