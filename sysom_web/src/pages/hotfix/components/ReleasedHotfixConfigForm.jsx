import ProForm, { ProFormText, ProFormSelect, ProFormSwitch, ProFormDatePicker } from '@ant-design/pro-form';
import React, { useRef, useState, useEffect } from 'react';
import { Button, Divider } from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import { postTask } from '../../diagnose/service'
import { submitReleasedHotfixInfo, filterReleasedHotfixInfo } from '../service'
import { ProFormTimePicker } from '@ant-design/pro-components';
import { UploadOutlined, DownloadOutlined, PlusOutlined } from '@ant-design/icons';
import { Upload, Form } from 'antd';

export default (props) => {
    const intl = useIntl();
    const formRef = useRef();
    const [readonlyone, setReadonlyOne] = useState(false);
    const [veroptionList,setVerOptionList] = useState([]);
    const [dataostype, setDataOsType] = useState([]);
    const { loading, error, run } = useRequest(submitReleasedHotfixInfo, {
        manual: true,
        onSuccess: (result, params) => {
            formRef.current.resetFields();
            props?.onSuccess?.(result, params);
        },
    });
    const KernelConfigChange = (e) => {
        setReadonlyOne(e);
    }
    const { filtering, filtererror, filterrun } = useRequest(filterReleasedHotfixInfo, {
        manual: true,
        onSuccess: (result, params) => {
            formRef.current.resetFields();
            props?.onSuccess?.(result, params);
        },
    });
    return (
        <ProCard>
            <ProForm
                onFinish={async (values) => {
                    run(values)
                }}
                formRef={formRef}
                submitter={{
                    submitButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                    resetButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                }}
                layout={"horizontal"}
                autoFocusFirstInput
            >
                <ProForm.Group>
                    <ProFormText
                        width="xd"
                        name="hotfix_id"
                        label={intl.formatMessage({id:'pages.hotfix.hotfixid', defaultMessage:'Hotfix ID'})}
                        tooltip={intl.formatMessage({
                            id:'pages.hotfix.tooltips.hotfixid', 
                            defaultMessage:'The Hotfix ID'
                        })}
                    />
                    <ProFormText
                        name="released_kernel_version"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_kernelverison',
                            defaultMessage: 'pages.hotfix.tooltips.released_kernelverison'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_kernel_version',
                            defaultMessage: 'Released kernel version'
                        })}
                    />
                    <ProFormSelect
                        name="serious"
                        valueEnum={{
                            0: "可选安装",
                            1: "建议安装",
                            2: "需要安装"
                        }}
                        width="xd"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.serious_level',
                            defaultMessage: 'The suggestion level of installing this hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.serious',
                            defaultMessage: 'Serious Level'
                        })}
                    />
                    <ProFormText
                        name="description"
                        width="md"
                        tooltip={intl.formatMessage({
                            id:'pages.hotfix.tooltips.description',
                            defaultMessage: 'The description of this hotfix.'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.description',
                            defaultMessage: 'Description'
                        })}
                    />
                    <ProFormSelect
                        name="subsystem"
                        width="xd"
                        options={[
                            {
                                label:"调度",
                                value:0,
                            },
                            {
                                label:"内存",
                                value:1,
                            },
                            {
                                label:"网络",
                                value:2,
                            },
                            {
                                label:"存储",
                                value:3,
                            },
                            {
                                label:"其他",
                                value:4,
                            }
                    ]}
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.fix_system',
                            defaultMessage: 'The Sub system this hotfix fixed',
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.fix_system',
                            defaultMessage: 'Fixed Subsystem'
                        })}
                    />
                    <ProFormText
                        name="download_link"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.downloadlink',
                            defaultMessage: 'Please input the download link of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.download_link',
                            defaultMessage: 'Download Link'
                        })}
                    />
                    <ProFormDatePicker
                        name="release_time"
                        width="md"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_time',
                            defaultMessage: 'Please input the released time of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_time',
                            defaultMessage: 'Released Time'
                        })}
                    />
                    <ProFormSwitch
                        style={{
                            marginBlockEnd: 16,
                        }}
                        name="deprecated"
                        initialValue={readonlyone}
                        label={intl.formatMessage({
                            id:'pages.hotfix.deprecated',
                            defaultMessage:'Deprecated'
                        })}
                        checked={readonlyone}
                        checkedChildren={intl.formatMessage({
                            id: 'pages.hotfix.yes',
                            defaultMessage: 'Yes'
                        })}
                        unCheckedChildren={intl.formatMessage({
                            id:'pages.hotfix.no',
                            defaultMessage: 'No'
                        })}
                        onChange={KernelConfigChange}
                    />
                    <ProFormText
                         name="deprecated_info"
                         hidden={false}
                         tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.deprecated_info',
                            defaultMessage: 'Please input the deprecated infomation of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.deprecated_info',
                            defaultMessage: 'Deprecated Info'
                        })}
                    />
                    <Button type="primary" htmlType="submit" loading={loading}>{intl.formatMessage({
                        id: 'pages.hotfix.submit',
                        defaultMessage: 'Submit'
                    })}</Button>
                    
                </ProForm.Group>
            </ProForm>
            <Divider/>
            {intl.formatMessage({
                        id: 'pages.hotfix.historicalfilter',
                        defaultMessage: 'Historical Hotfix Information Filter'
                    })}
            <ProForm>
            <ProForm.Group>
                <ProFormText
                        name="filter_hotfix_id"
                        width="xd"
                        tooltip={intl.formatMessage({
                            id:'pages.hotfix.tooltips.hotfixid',
                            defaultMessage: 'The hotfix ID.'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.hotfixid',
                            defaultMessage: 'Hotfix ID'
                        })}
                    />
                <ProFormText
                        name="released_kernel_version"
                        width="xd"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_kernelverison',
                            defaultMessage: 'pages.hotfix.tooltips.released_kernelverison'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_kernel_version',
                            defaultMessage: 'Released kernel version'
                        })}
                    />
                <ProFormSelect
                        name="serious"
                        options={[{label:"可选安装",value:0},{label:"建议安装", value:1},{label:"需要安装", value:2},{label:"全部", value:-1}]}
                        width="xd"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.serious_level',
                            defaultMessage: 'The suggestion level of installing this hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.serious',
                            defaultMessage: 'Serious Level'
                        })}
                    />
                <ProFormSelect
                        name="subsystem"
                        width="xd"
                        options={[
                            {
                                label:"调度",
                                value:0
                            },
                            {
                                label:"内存",
                                value:1,
                            },
                            {
                                label:"网络",
                                value:2,
                            },
                            {
                                label:"存储",
                                value:3,
                            },
                            {
                                label:"其他",
                                value:4,
                            },
                            {
                                label:"全部",
                                value:-1,
                            }
                    ]}
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.fix_system',
                            defaultMessage: 'The Sub system this hotfix fixed',
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.fix_system',
                            defaultMessage: 'Fixed Subsystem'
                        })}
                    />
                <ProFormDatePicker
                        name="release_time"
                        width="xd"
                        tooltip={intl.formatMessage({
                            id: 'pages.hotfix.tooltips.released_time',
                            defaultMessage: 'Please input the released time of this released hotfix'
                        })}
                        label={intl.formatMessage({
                            id: 'pages.hotfix.released_time',
                            defaultMessage: 'Released Time'
                        })}
                    />
            </ProForm.Group>
            </ProForm>
        </ProCard>
    )
}
