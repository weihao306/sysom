// @ts-ignore

/* eslint-disable */
import {
  request
} from 'umi';

export async function getAudit(params, options) {
  const token = localStorage.getItem('token');
  const msg = await request('/api/v1/log/audit', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
  return msg
}


export async function getResponseCode(params, options) {
  const token = localStorage.getItem('token');
  const result = await request('/api/v1/response_code/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    params: params,
    ...(options || {}),
  });
  return result
}

export async function getTaskList(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/tasks/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
}

export async function getAlarmList(params, options) {
  const token = localStorage.getItem('token');
  const msg = await request('/api/v1/alarm/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
  return msg
}

export async function changeAlarmIsReadHandler(alarmId, body, options) {
  const token = localStorage.getItem('token')
  return await request(`/api/v1/alarm/${alarmId}/`, {
    method: 'PATCH',
    data: body,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token
    },
    ...(options || {})
  })
}

export async function getNodeLogList(params, options) {
  const token = localStorage.getItem('token');
  const result = await request('/api/v1/log/node', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  })
  return result
}

/**
 * 根因分析
 * curl -X POST 'http://127.0.0.1/api/v1/rca/rca_call'
 *  -H 'Content-Type: application/json'
 *  -d  '{"rca_type":"rca","time":"2023-07-30 14:12:23","base_item":"test_RT","machine_ip":"192.168.0.130"}'
 */
export async function rootCauseAnalysis(data, options) {
  const token = localStorage.getItem('token');
  const result = await request('/api/v1/rca/rca_call', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: data,
    ...(options || {}),
  })
  return result;
}