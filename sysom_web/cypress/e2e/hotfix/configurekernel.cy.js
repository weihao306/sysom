/* ==== Test Created with Cypress Studio ==== */
/**
 * This js is use for automaticly test the function 
 * of kernel version and os type configuration test.
 */
it('ostest', function() {
    cy.login()
    /** test for OSType configure */
    cy.visit('/hotfix/version/config');
    cy.get('#os_type').clear('a');
    cy.get('#os_type').type('alinux3');
    cy.get('#sync_conf').click();
    cy.get('#src_pkg_mark').click();
    cy.get('#source_repo').clear('http://mirrors.aliyun.com/alinux/3/plus/source/SRPMS/');
    cy.get('#source_repo').type('http://mirrors.aliyun.com/alinux/3/plus/source/SRPMS/');
    cy.get('#image').clear('registry.cn-hangzhou.aliyuncs.com/sysom/hotfix_building:alinux3_v1.0');
    cy.get('#image').type('registry.cn-hangzhou.aliyuncs.com/sysom/hotfix_building:alinux3_v1.0');
    cy.get('#source_devel').clear('http://mirrors.aliyun.com/alinux/3/plus/x86_64/Packages/');
    cy.get('#source_devel').type('http://mirrors.aliyun.com/alinux/3/plus/x86_64/Packages/');
    cy.get('#source_debuginfo').clear('http://mirrors.aliyun.com/alinux/3/plus/x86_64/debug/');
    cy.get('#source_debuginfo').type('http://mirrors.aliyun.com/alinux/3/plus/x86_64/debug/');
    cy.get(':nth-child(8) > .ant-btn').click();
    cy.wait(1000)
    cy.get(':nth-child(1) > .ant-pro-table-list-toolbar-setting-item > :nth-child(1) > .anticon > svg').click();
    cy.get('.ant-tag').click();
    /** find the refresh button */
    cy.get(':nth-child(1) > .ant-pro-table-list-toolbar-setting-item > :nth-child(1) > .anticon > svg').click();
    /** change information of an OSType */
    cy.get('table').within(() => {
      cy.get('tr').eq(1).contains('编辑').click({force: true})
    })
	cy.get('tr:first-child td:first-child input').clear().type('x86');
	cy.get('table').within(() => {
		cy.get('tr').eq(1).contains('保存').click({force: true})
	  })
	//cy.get('.ant-typography').contains("保存").click()
    /* test for Kernel Version Page */
    cy.visit('/hotfix/version/customize');
    cy.get('#os_type').click();
    cy.get('.ant-select-item-option-content').click();
    cy.get('#kernel_version').clear('a');
    cy.get('#kernel_version').type('a');
    cy.get('#source').clear('b');
    cy.get('#source').type('b');
    cy.get('#devel_link').clear('c');
    cy.get('#devel_link').type('c');
    cy.get('#debuginfo_link').clear('d');
    cy.get('#debuginfo_link').type('d');
    cy.get('#image').type('image')
	cy.get(':nth-child(8) > .ant-btn').click();
	/* fix the record of a kernel version */
    cy.wait(1000)
    cy.get('table').within(() => {
      cy.get('tr').eq(1).contains('编辑').click({force: true})
    })
	cy.get('tr:first-child td:first-child input').clear().type('ccccc');
	cy.get('table').within(() => {
		cy.get('tr').eq(1).contains('保存').click({force: true})
	  })
	/** delete this record */
    cy.get('table').within(() => {
      cy.get('tr').eq(1).contains('删除').click({force: true})
    })
    cy.get('.ant-popover-buttons').contains('OK').click();
  });
  
