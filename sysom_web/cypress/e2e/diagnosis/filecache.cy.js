/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- filecache", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke filecache all diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/memory/filecache",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
                //"value": "",
                "type": "all"
            },

            // 诊断结果处理此处判断诊断的结果数据是否符合预期）
            /*{
                "code":0,
                "err_msg":"",
                "result":{
                    "podmem":{
                        "data":[
                            {
                                "key":0,
                                "POD":"mysql-80-136-fc79d5656-db5dk",
                                "Container":"19b260e14d6c1",
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002122",
                                "Filesize":1048827,
                                "Cached":1048828,
                                "ActiveCached":0,
                                "InActiveCached":10268,
                                "ext":"s:0,d:0"
                            },
                            {
                                "key":1,
                                "POD":"mysql-80-136-fc79d5656-db5dk",
                                "Container":"19b260e14d6c1",
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002121",
                                "Filesize":1048827,
                                "Cached":1048828,
                                "ActiveCached":0,
                                "InActiveCached":10344,
                                "ext":"s:0,d:0"
                            },
                            ...
                            {
                                "key":0,
                                "POD":"mysql-57-136-6948898f5d-5qrhz",
                                "Container":"bcf1a8a097d52",
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-57-136/ibdata1",
                                "Filesize":667648,
                                "Cached":579720,
                                "ActiveCached":0,
                                "InActiveCached":5748,
                                "ext":"s:0,d:0"
                            },
                            {
                                "key":0,
                                "POD":"mysql-56-136-6c854b66f6-6nhnf",
                                "Container":"682554ea93e4f",
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-56-136/ibdata1",
                                "Filesize":602112,
                                "Cached":444932,
                                "ActiveCached":0,
                                "InActiveCached":4560,
                                "ext":"s:0,d:0"
                            }
                        ]
                    },
                    "summary":"success"
                }
            }*/
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                //cy.get('.ant-table-thead').should("contain.text", "请求时间")
                /* ==== End Cypress Studio ==== */
                cy.wait(1000)
                cy.get('.ant-pro-card-title > div').scrollIntoView()
                cy.get('.ant-pro-card-title > div').should('contain.text', '诊断结果')
                cy.get('.ant-table-content').last().find('table').then(($el) => {
                    if ($el.text().includes("No data")) {
                        cy.wrap($el).contains("No data")
                    } else {
                        const dataList = result.result?.podmem?.data
                        if (dataList && dataList.length > 0) {
                            const rowItem = dataList[0]
                            const rowNames = Object.keys(rowItem)
                            const thNames = rowNames.filter(item => item != "key")
                            
                            cy.get('table').last().find('thead').find('tr').find('th').should('have.length.gte', 6)
                            thNames.forEach((name, index) => { 
                              cy.get('table').last().find('thead').find('tr').find('th').eq(index).should('contain.text', name)
                            })
                        }
                    }
                })

            })
        /* ==== Generated with Cypress Studio ==== */

        /* ==== End Cypress Studio ==== */
    })

    it("Invoke filecache host diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/memory/filecache",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
                //"value": "",
                "type": "host"
            },

            // 诊断结果处理此处判断诊断的结果数据是否符合预期）
            /*{
                "code":0,
                "err_msg":"",
                "result":{
                    "podmem":{
                        "data":[
                            {
                                "key":0,
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002127",
                                "Filesize":1048827,
                                "Cached":1048828,
                                "ActiveCached":0,
                                "InActiveCached":10596,
                                "ext":"s:0,d:0"
                            },
                            {
                                "key":1,
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002126",
                                "Filesize":1048827,
                                "Cached":1048828,
                                "ActiveCached":0,
                                "InActiveCached":10676,
                                "ext":"s:0,d:0"
                            },
                            {
                                "key":2,
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002125",
                                "Filesize":1048827,
                                "Cached":1048828,
                                "ActiveCached":0,
                                "InActiveCached":10472,
                                "ext":"s:0,d:0"
                            },
                            ...
                            {
                                "key":9,
                                "Filename":"/var/lib/kubelet/var/lib/kubelet/plugins/kubernetes.io/csi/diskplugin.csi.alibabacloud.com/35410fe69184f91b96d483a18d0fae0aa381c6d4e0fb838bb9b04837781fb1d3/globalmount/mysql-80-136/binlog.002120",
                                "Filesize":1048827,
                                "Cached":423996,
                                "ActiveCached":0,
                                "InActiveCached":4220,
                                "ext":"s:0,d:0"
                            }
                        ]
                    },
                    "summary":"success"
                }
            }*/
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                //cy.get('.ant-table-thead').should("contain.text", "请求时间")
                /* ==== End Cypress Studio ==== */
                cy.get('.ant-pro-card-title > div').scrollIntoView()
                cy.get('.ant-pro-card-title > div').should('contain.text', '诊断结果')
                cy.get('.ant-table-content').last().find('table').then(($el) => {
                    if ($el.text().includes("No data")) {
                        cy.wrap($el).contains("No data")
                    } else {
                        const dataList = result.result?.podmem?.data
                        if (dataList && dataList.length > 0) {
                            const rowItem = dataList[0]
                            const rowNames = Object.keys(rowItem)
                            const thNames = rowNames.filter(item => item != "key")
                            
                            cy.get('table').last().find('thead').find('tr').find('th').should('have.length.gte', 6)
                            thNames.forEach((name, index) => { 
                              cy.get('table').last().find('thead').find('tr').find('th').eq(index).should('contain.text', name)
                            })
                        }
                    }
                })
            })
    })
})
