/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- rtdelay", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke rtdelay diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/link/rtdelay",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
                "pid": "-1",
                "time": "2"
            },

            // 诊断结果处理此处判断诊断的结果数据是否符合预期）
            // {
            // "id": 1211,
            // "created_at": "2023-08-30 14:47:19",
            // "updated_at": "2023-08-30 14:47:19",
            // "task_id": "coQpsZ25",
            // "status": "Success",
            // "service_name": "rtdelay",
            // "code": 0,
            // "err_msg": "",
            // "result": {
            //     "request_set": {
            //     "data": [
            //         {
            //         "请求时间": "2023-08-30 14:43:12",
            //         "RT时延": 4593888,
            //         "oncpu": 1973,
            //         "运行队列时间": 27,
            //         "存储时间": 2047963,
            //         "纯网络时延": 68,
            //         "服务器处理时间": 1000727,
            //         "futex": 119,
            //         "mutex": 1543004,
            //         "其他": 0
            //         }
            //     ]
            //     }
            // },
            // "params": {
            //     "instance": "127.0.0.1",
            //     "service_name": "rtdelay"
            // },
            // "created_by": 1,
            // "url": "/diagnose/detail/coQpsZ25"
            // }
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                
                cy.diagnosisTaskResultHandler(result, ()=> {
                    cy.get('.ant-table-thead').should("contain.text", "请求时间")
                })
                /* ==== End Cypress Studio ==== */
            })
    })
})
