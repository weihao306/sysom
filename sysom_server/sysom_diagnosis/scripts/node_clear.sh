#!/bin/bash -x
RESOURCE_DIR=${NODE_HOME}/${SERVICE_NAME}

main()
{
    rpm -qa | grep sysak
    if [ $? -eq 0 ]; then
        yum erase -y `rpm -qa | grep sysak`
    fi
    rm -rf ${RESOURCE_DIR}
    exit 0
}

main
