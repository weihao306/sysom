"""
Time                2023/07/21 10:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                get_release_pre.py
Description:
"""
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask, FileItem


class PreProcessor(DiagnosisPreProcessor):
    """Get release info diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        instance = params.get("instance", "")
        return DiagnosisTask(
            jobs=[
                DiagnosisJob(instance=instance, cmd="uname -r", fetch_file_list=[
                    FileItem("os-release", remote_path="/etc/os-release")
                ])
            ],
            in_order=False,
        )
