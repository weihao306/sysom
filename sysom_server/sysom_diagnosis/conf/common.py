import os
from pathlib import Path
from sysom_utils import ConfigParser, CecTarget, SysomFramework


BASE_DIR = Path(__file__).resolve().parent.parent

##################################################################
# Load yaml config first
##################################################################
YAML_GLOBAL_CONFIG_PATH = f"{BASE_DIR.parent.parent}/conf/config.yml"
YAML_SERVICE_CONFIG_PATH = f"{BASE_DIR}/config.yml"

YAML_CONFIG = ConfigParser(YAML_GLOBAL_CONFIG_PATH, YAML_SERVICE_CONFIG_PATH)

SysomFramework.init(YAML_CONFIG)

##########################################################################################
# Diagnosis Service config
##########################################################################################

SCRIPTS_DIR = os.path.join(BASE_DIR, 'service_scripts')

DEFAULT_CHANNEL = YAML_CONFIG.get_service_config().get("default_channel", "ssh")
IGNORE_TOOL_CHECK_CHANNELS = YAML_CONFIG.get_service_config().get("ignore_tool_check_channels", ["offline"])

##################################################################
# Cec settings
##################################################################
SYSOM_CEC_PRODUCER_URL = YAML_CONFIG.get_cec_url(CecTarget.PRODUCER)
# 诊断模块消费组
SYSOM_CEC_DIAGNOSIS_CONSUMER_GROUP = "SYSOM_CEC_DIAGNOSIS_CONSUMER_GROUP"
# 诊断任务下发主题（由 View -> Executor）
SYSOM_CEC_DIAGNOSIS_TASK_DISPATCH_TOPIC = "SYSOM_CEC_DIAGNOSIS_TASK_DISPATCH_TOPIC"
# 离线诊断回传主题（回传诊断命令执行的原始结果）
SYSOM_CEC_OFFLINE_ORIGIN_DIAGNOSIS_RESULT_TOPIC = "SYSOM_CEC_OFFLINE_ORIGIN_DIAGNOSIS_RESULT_TOPIC"

# 诊断任务创建成功（已执行前处理脚本）
SYSOM_CEC_DIAGNOSIS_TASK_CREATED = "SYSOM_CEC_DIAGNOSIS_TASK_CREATED"

# channl_job SDK 需要的url
CHANNEL_JOB_URL = YAML_CONFIG.get_local_channel_job_url()

##########################################################################################
# Django Config
##########################################################################################

SECRET_KEY = YAML_CONFIG.get_server_config().jwt.get("SECRET_KEY", "")

# JWT Token Decode DIR
JWT_TOKEN_DECODE_DIR = os.path.join(BASE_DIR, 'lib', 'decode')
if not os.path.exists(JWT_TOKEN_DECODE_DIR):
    os.makedirs(JWT_TOKEN_DECODE_DIR)

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'apps.task',

    'rest_framework',
    'corsheaders',
    'django.contrib.staticfiles',
    'drf_yasg',  # 在线API文档
    'django_filters',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
]

DEBUG = True

# Mysql数据库
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': YAML_CONFIG.get_server_config().db.mysql.database,
        'USER': YAML_CONFIG.get_server_config().db.mysql.user,
        'PASSWORD': YAML_CONFIG.get_server_config().db.mysql.password,
        'HOST': YAML_CONFIG.get_server_config().db.mysql.host,
        'PORT': YAML_CONFIG.get_server_config().db.mysql.port,
    }
}

ROOT_URLCONF = 'sysom_diagnosis.urls'

WSGI_APPLICATION = 'sysom_diagnosis.wsgi.application'
ASGI_APPLICATION = 'sysom_diagnosis.asgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

SYSOM_API_URL = 'http://127.0.0.1:7001'

LANGUAGE_CODE = 'zh-hans'
TIME_ZONE = YAML_CONFIG.get_global_config().timezone
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'index/status')

# rest_framework settings
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        # 'rest_framework.permissions.IsAuthenticated'
    ),
    # 'DEFAULT_AUTHENTICATION_CLASSES': [
    #     'apps.accounts.authentication.Authentication'
    # ],
    'UNAUTHENTICATED_USER': None,
    'DEFAULT_VERSIONING_CLASS': "rest_framework.versioning.URLPathVersioning",
    'DEFAULT_VERSION': 'v1',  # 默认版本
    'ALLOWED_VERSIONS': ['v1', 'v2'],  # 允许的版本
    'VERSION_PARAM': 'version',

    # 'DEFAULT_RENDERER_CLASSES': (
    #     'lib.renderers.SysomJsonRender',
    # ),
    'DEFAULT_PAGINATION_CLASS': 'lib.paginations.Pagination',
    'UNICODE_JSON': True,
    'EXCEPTION_HANDLER': 'lib.exception.exception_handler'
}

##########################################################################################
# Check task interval thread config
##########################################################################################
CHECK_INTERVAL = YAML_CONFIG.get_service_config().checkinterval               # check status running task interval 60 seconds
TASK_EXECUTE_TIMEOUT = YAML_CONFIG.get_service_config().taskexecutetimeout    # default status running task exectue 1 minutes timeout


##########################################################################################
# Prometheus config
##########################################################################################
prometheus_config = YAML_CONFIG.get_server_config().db.prometheus

PROMETHEUS_DATABASE_URL = (
    f"prometheus://{prometheus_config.host}:{prometheus_config.port}"
)
