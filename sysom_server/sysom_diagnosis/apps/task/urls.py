# -*- encoding: utf-8 -*-
"""
@File    : urls.py
@Time    : 2021/11/22 10:38
@Author  : DM
@Software: PyCharm
"""
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter
from apps.task import views

router = DefaultRouter()

router.register('tasks', views.TaskAPIView)

urlpatterns = [
    path('api/v1/tasks/task_hook/', views.TaskAPIView.as_view({'post': 'task_hook'})),
    path('api/v1/tasks/sbs_task_create/', views.TaskAPIView.as_view({'post': 'sbs_task_create'})),
    path('api/v1/tasks/sbs_task_result/', views.TaskAPIView.as_view({'post': 'sbs_task_result'})),
    path('api/v1/tasks/offline_import/', views.TaskAPIView.as_view({'post': 'offline_import'})),
    path('api/v1/tasks/health_check/', views.TaskAPIView.as_view({'get': 'health_check'})),
    path('api/v1/tasks/host/', views.TaskAPIView.as_view({'get': 'get_host'})),
    path('api/v2/tasks/', views.TaskAPIView.as_view({'post': 'create_task_v2'})),
    path('api/v1/', include(router.urls))
]
