import os
import re
import tempfile
import pandas as pd
import functools
from typing import List
from datetime import datetime
from pandas.core.frame import DataFrame
from pandas._libs.tslibs.timestamps import Timestamp
from clogger import logger
from django.utils import timezone
from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework import serializers
from apps.hotfix.models import HotfixModel, OSTypeModel, KernelVersionModel, ReleasedHotfixListModule

class HotfixSerializer(serializers.ModelSerializer):

    class Meta:
        model = HotfixModel
        fields = '__all__' # fields 指定从数据库返回的字段
    

class OSTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = OSTypeModel
        fields = '__all__'

class KernelSerializer(serializers.ModelSerializer):

    class Meta:
        model = KernelVersionModel
        fields = '__all__'

class ReleasedHotfixSerializer(serializers.ModelSerializer):

    deprecated = serializers.SerializerMethodField()
    modified_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    released_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = ReleasedHotfixListModule
        fields = '__all__'

    def get_deprecated(self, obj): return bool(obj.deprecated)


class CreateReleasedHotfixSerializer(serializers.ModelSerializer):

    deprecated = serializers.BooleanField(default=False)
    deprecated_info = serializers.CharField(default="")

    class Meta:
        model = ReleasedHotfixListModule
        exclude = ['modified_time', 'modified_user']

    def validate(self, attrs):
        deprecated_info = attrs.get('deprecated_info', "")
        deprecated = attrs.get('deprecated', False)
        if not deprecated and len(deprecated_info) > 0:
            raise serializers.ValidationError('Field deprecated is false, deprecated_info is empty!')

        if deprecated and len(deprecated_info) == 0:
            raise serializers.ValidationError("Field deprecated is true, field deprecated_info required!")

        return super().validate(attrs)

    def validate_hotfix_id(self, attrs):
        instance = ReleasedHotfixListModule.objects.filter(hotfix_id=attrs).first()
        if instance is None:
            return super().validate(attrs)
        else:
            raise serializers.ValidationError(detail='Field hotfix_id does not exist')

    def validate_released_kernel_version(self, attrs):
        instance = ReleasedHotfixListModule.objects.filter(released_kernel_version=attrs).first()
        if instance is None:
            return super().validate(attrs)
        else:
            raise serializers.ValidationError(detail='Field released_kernel_version does not exist')


class UpdateReleasedHotfixSerializer(serializers.ModelSerializer):
    released_kernel_version = serializers.CharField(required=False)
    hotfix_id = serializers.CharField(required=False)
    released_time = serializers.DateTimeField(required=False)

    class Meta:
        model = ReleasedHotfixListModule
        exclude = ['modified_time', 'modified_user']

    def validate_hotfix_id(self, attrs):
        """Filter the data named "hotfix_id" so that it cannot be repeated in the database"""
        instance = ReleasedHotfixListModule.objects.filter(hotfix_id=attrs).first()
        if instance:
            raise serializers.ValidationError(f"field hotfix_id: {attrs} Exist")
        else:
            return attrs

    def validate(self, attrs):
        """
        Filter the data named "released_kernel_version"
        so that it cannot be repeated in the database
        """
        hotfix_id=attrs.get("hotfix_id",None)
        released_kernel_version=attrs.get("released_kernel_version",None)
        if hotfix_id and released_kernel_version:
            instance_kernel_version=ReleasedHotfixListModule.objects.filter(**attrs)
            if instance_kernel_version:
                raise serializers.ValidationError(f"released_kernel_version:{released_kernel_version} is already existed!") 
        return attrs


class UpdatePutReleasedHotfixSerializer(serializers.ModelSerializer):
    deprecated = serializers.BooleanField()

    class Meta:
        model = ReleasedHotfixListModule
        exclude = ['modified_time', 'modified_user']

    def validate_deprecated(self, attrs):
        return 1 if attrs else 0
    
    def validate(self, attrs):
        deprecated_info = attrs.get('deprecated_info', "")
        deprecated = attrs.get('deprecated', False)
        if not deprecated and len(deprecated_info) > 0:
            raise serializers.ValidationError('Field deprecated is false, deprecated_info is empty!')

        if deprecated and len(deprecated_info) == 0:
            raise serializers.ValidationError("Field deprecated is true, field deprecated_info required!")
        return super().validate(attrs)


class BulkImportHotfixReleasedSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)

    def __init__(self, instance=None, data=..., **kwargs):
        super().__init__(instance, data, **kwargs)
        self._file: InMemoryUploadedFile = None
        self._save_temporary_file = None
        self._suffix: str = None
        self._save_file_path = None
        self._default_save_path = '/tmp/'
        self._suffixs = ['xls', 'csv', 'xlsx']
        self._file_header_fields = [
            "hotfix_id", "released_time", "released_kernel_version"
        ]
        self._error_download_link = []
        self._action = {
            "csv": functools.partial(pd.read_csv),
            "xls": functools.partial(pd.read_excel, keep_default_na=False, engine="xlrd"),
            "xlsx": functools.partial(pd.read_excel, keep_default_na=False, engine="openpyxl"),
        }

    def validate_file(self, file: InMemoryUploadedFile):
        """validate param `file`"""
        file_name = file.name
        self._suffix = file_name.split('.')[-1]

        if self._suffix not in self._suffixs:
            raise serializers.ValidationError('file suffix invaild!')
        self._file = file

        self._save_upload_file()
        try:
            context = self._file_parse()
        except Exception as e:
            raise serializers.ValidationError(f'parse file faid! Error: {e}')
        finally:
            self.close_file()
        return context

    def _save_upload_file(self):
        """Save the file to a temporary file"""
        self._save_temporary_file = tempfile.NamedTemporaryFile(
            dir=self._default_save_path,
            suffix=f".{self._suffix}",
        )
        self._save_file_path = os.path.join(self._default_save_path, self._save_temporary_file.name)

        with open(self._save_file_path, 'wb') as f:
            for chunk in self._file.chunks(chunk_size=1024):
                f.write(chunk)

    def _file_parse(self) -> DataFrame:
        """parse the file"""
        if self._suffix is None:
            raise serializers.ValidationError("file suffix is not none!")

        if self._save_file_path is None:
            raise serializers.ValidationError('file save file is not none!')
        return self._action[self._suffix](self._save_file_path)

    def _kernel_version_or_download_link_map(
        self,
        kernel_versions: List[str],
        urls: List[str]
    ) -> List[dict]:
        '''
        @Description: Find the corresponding download link in the
        download links by using the key characters in the kernel-
        version
        @Params `kernel_version`: Type(List) Store the kernel ve-
        rsion list
        @Params `urls`: Type(List) Store the donwload_link list
        @return {*}
        '''
        _version_download_link_list = list()
        reg_compile = re.compile(
            "(\d+)\.(\d+)\.(\d+)-(\d+)\.?(\d+){0,1}(?!_)"
            )

        def _filter(marks: List[str], url: str):
            '''
            @Description: 
            @param {List} marks
            @param {str} url
            @return {*}
            '''
            flag = True
            for mark in marks:
                if mark not in url:
                    flag = False
                    break
            return flag

        def _filter_subset(filter_sub : List, kernel_version_flag):
            """
            filter out the exact downloadlink
            @ filter_flag: ['http:', '', 'yum.tbsite.net', 'alios', '7u2', 'os', 'aarch64',  ※'kernel-hotfix-CVE-2023-0461-5.10.134-13' ※, 
            'kernel-hotfix-CVE-2023-0461-5.10.134-13-1.0-20230316152330.al8.aarch64.rpm']
            """
            for kernel_link in filter_sub:
                filter_flag = kernel_link.split("/")[7]
                filter_flag_fin = re.search(reg_compile, filter_flag).group(0)
                if kernel_version_flag == filter_flag_fin:
                    return kernel_link

        for kernel_version in kernel_versions:
            v = re.search(reg_compile, kernel_version).groups()
            version_s = re.sub(reg_compile, "", kernel_version)
            marks = version_s.split(".")[1:]
            marks.append(v)
            marks = re.findall(r"([\d\-\.]+)\.((\w+)\.)+(\w+)", kernel_version)[0] # tuple
            # [5.10.23-5 , al8 , x86_64]
            filter_sub = list(filter(lambda x: _filter(marks, x), urls))
            if len(filter_sub) >= 1:
                subset_result = filter_sub[0] if len(filter_sub) == 1 else _filter_subset(filter_sub, marks[0]) #!!!
                _version_download_link_list.append((kernel_version, subset_result))
        return _version_download_link_list

    def create(self, validated_data):
        """Save the data to the database"""
        def _released_time(released_time):
            """
            @Description: released time handler
            @param {*} released_time: `released_time` type is string or DateTime or empty string
            @return {*} released_time: type(datetime)
            """
            if isinstance(released_time, str) and released_time != "":
                released_time = datetime.strptime(
                    released_time.split(".")[0], "%Y-%m-%d %H:%M:%S"
                )
                released_time = timezone.get_current_timezone().localize(released_time)
            elif isinstance(released_time, Timestamp):
                released_time = timezone.get_current_timezone().localize(released_time)
            else:
                released_time = timezone.now()
            return released_time
        
        def _structure_released_hotfix(**data_rows: dict) -> List[ReleasedHotfixListModule]:
            """
            initizaer released hotfix model
            """
            def _release_kernel_version_model(hotfix_id, released_kernel_version, released_time, **kwargs):
                try:
                    ReleasedHotfixListModule.objects.get(
                        hotfix_id=hotfix_id, released_kernel_version=released_kernel_version)
                    return None
                except ReleasedHotfixListModule.DoesNotExist:
                    return ReleasedHotfixListModule(
                            hotfix_id=hotfix_id, released_kernel_version=released_kernel_version,
                            released_time=released_time, **kwargs
                        )

            released_hotfix_list = list()
            hotfix_id = data_rows.pop("hotfix_id")
            released_time = _released_time(data_rows.pop("released_time"))
            released_kernel_version: str = data_rows.pop("released_kernel_version")

            model_fields_list = [
                field.name
                for field in ReleasedHotfixListModule._meta.get_fields()
            ]

            _other_kwargs = {
                k: v
                for k, v in data_rows.items()
                if k in model_fields_list\
                    and (
                    v is not None 
                    and
                    v != ""
                )
            }

            kernel_versions = released_kernel_version.split(",")
            if len(kernel_versions) == 1:
                _model = _release_kernel_version_model(
                    hotfix_id, released_kernel_version, released_time, **_other_kwargs
                )
                released_hotfix_list.append(_model) if _model is not None else ...
            else:
                download_link: str = _other_kwargs.get("download_link")
                download_links = download_link.split(" ")
                if download_link is None or len(download_links) == 1:
                    for kernel_version in kernel_versions:
                        _model = _release_kernel_version_model(
                            hotfix_id, kernel_version, released_time, **_other_kwargs
                        )
                        released_hotfix_list.append(_model) if _model is not None else ...
                else:
                    for item in self._kernel_version_or_download_link_map(
                        kernel_versions, download_links
                    ):
                        kernel_version, download_link = item
                        _other_kwargs.update({"download_link": download_link})
                        _model = _release_kernel_version_model(
                                hotfix_id, kernel_version, released_time, **_other_kwargs
                            )
                        released_hotfix_list.append(_model) if _model is not None else ...
            return released_hotfix_list

        excel_file_content: DataFrame = validated_data.get("file")
        for param in [
            field for field in self._file_header_fields\
                if field not in excel_file_content.columns.values
        ]:
            raise serializers.ValidationError(
                f"excel tabel header required include {param} field"
            )
        
        for data in excel_file_content.to_dict(orient='records'):
            ReleasedHotfixListModule.objects.bulk_create(
                _structure_released_hotfix(**data)
            )
        return []

    def close_file(self):
        """delete the temporary file!"""
        if self._save_temporary_file:
                self._save_temporary_file.close()
