import time

import pytz
import conf.settings as settings
from typing import List
from metric_reader.metric_reader import MetricReader, RangeQueryTask
from clogger import logger
from datetime import datetime, date


def get_today_zero_ts() -> float:
    today = date.today()
    today_tuple = today.timetuple()
    today_zero_ts = time.mktime(today_tuple)
    return today_zero_ts


def ts_2_hour(ts: float) -> int:
    return datetime.fromtimestamp(ts, pytz.timezone("Asia/Shanghai")).hour


def collect_all_clusters(metric_reader: MetricReader) -> List[str]:
    cluster_list = []
    res = metric_reader.get_label_values("cluster")
    if len(res.data) <= 0:
        logger.warning("Collect all cluster failed!")
        return cluster_list
    return [item for item in res.data]


def collect_instances_of_cluster(
    cluster_id: str, metric_reader: MetricReader, interval: int
) -> List[str]:
    """Collect all instances of specific cluster

    Use "sysom_proc_cpu_total" metric to collect all pods
    of specific instance, need to make sure the metric has been correctlly
    exported (similar to grafana variables).

    Args:
        instance_id: instance id
        metric_reader: MetricReader instance of metric_reader sdk
        interval: time interval of query

    Returns:
        List of instances
    """

    instances_list = []

    task = RangeQueryTask(
        settings.TABLE_PROC_CPU_TOTAL,
        start_time=time.time() - interval,
        end_time=time.time(),
    ).append_equal_filter("mode", "total")

    if cluster_id != "default":
        task.append_equal_filter("cluster", cluster_id)

    node_metric_res = metric_reader.range_query([task])
    if len(node_metric_res.data) <= 0:
        logger.error(f"Collect instances of {cluster_id} info: no instances found!")
        return instances_list

    try:
        for i in range(len(node_metric_res.data)):
            labels = node_metric_res.data[i].to_dict()["labels"]
            if settings.NODE_LABEL in labels:
                instances_list.append(labels[settings.NODE_LABEL])
    except Exception as e:
        raise e

    return list(set(instances_list))


def generate_unique_key(*args, **kwargs) -> str:
    val_list = []
    for arg in args:
        val_list.append(arg)

    for value in kwargs.values():
        val_list.append(str(value))
    return "-".join(val_list)
