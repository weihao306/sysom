sysom_item_prio = {
    "sysom_proc_cpu_total-user":{"meaning":"系统用户态cpu占用率","field":"调度","priority":1,"level":1,"threshold":95,"tool":"loadtask","fix_advice":"限制用户态cpu占用率，迁移或停止部分业务","affected_items":[],"affecting_items":[],"cause":"业务进程cpu负载过高"},
    "sysom_proc_pkt_status-retrans":{"meaning":"协议栈溢出次数","field":"网络","priority":1,"level":2,"threshold":-1,"tool":"retran","fix_advice":"内存泄漏诊断，降低或迁移网络流量","affected_items":[],"affecting_items":[],"cause":"网络拥堵：当网络中的流量超过了其带宽容量时，就会导致网络拥塞和延迟。在这种情况下，数据包可能会丢失或需要进行重传；网络拓扑问题：网络结构、协议不当等导致数据包丢失或路由错误，需要进行重传；网络硬件故障：路由器、交换机、网卡等硬件设备故障导致数据包丢失或传输错误，需要进行重传"},
    "sysom_sock_stat-tcp_mem":{"meaning":"tcp socket 内存使用量，含收发缓冲区队列","field":"内存","priority":1,"level":3,"threshold":-1,"tool":"memleak","fix_advice":"调高tcp_mem的水线（/proc/sys/net/ipv4/tcp_mem），需进一步评估测试该配置","affected_items":[],"affecting_items":[],"cause":"tcp_mem内存占用过多、水线设置不合理、收发包处理不及时或socket内存泄漏"}

}
