# -*- coding: utf-8 -*- #
"""
Time                2023/09/19 15:41
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from typing import Optional
from app import models
from sysom_utils import BaseQueryParams


class NodeLogQueryParams(BaseQueryParams):
    # 1. 指定要查询的模型
    __modelclass__ = models.NodeLog

    # 2. 定义排序字段
    sort__: str = "-ts"

    # 3. 定义支持用于过滤的参数
    instance: Optional[str] = None
    event_type: Optional[str] = None


class AuditLogQueryParams(BaseQueryParams):
    # 1. 指定要查询的模型
    __modelclass__ = models.AuditLog

    # 2. 定义排序字段
    sort__: str = "-ts"

    # 3. 定义支持用于过滤的参数
    methond: Optional[str] = None
    ip: Optional[str] = None
    status: Optional[str] = None
    request_type: Optional[str] = None
    user: Optional[str] = None
    path: Optional[str] = None
    browser_agent: Optional[str] = None
    handler: Optional[str] = None
