# -*- coding: utf-8 -*- #
"""
Time                2023/11/29 10:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""
from pathlib import Path
from sysom_utils import ConfigParser, SysomFramework

BASE_DIR = Path(__file__).resolve().parent.parent

##################################################################
# Load yaml config first
##################################################################
YAML_GLOBAL_CONFIG_PATH = f"{BASE_DIR.parent.parent}/conf/config.yml"
YAML_SERVICE_CONFIG_PATH = f"{BASE_DIR}/config.yml"

YAML_CONFIG = ConfigParser(YAML_GLOBAL_CONFIG_PATH, YAML_SERVICE_CONFIG_PATH)

mysql_config = YAML_CONFIG.get_server_config().db.mysql
service_config = YAML_CONFIG.get_service_config()

SysomFramework.init(YAML_CONFIG)

##################################################################
# fastapi config
##################################################################
SQLALCHEMY_DATABASE_URL = (
    f"{mysql_config.dialect}+{mysql_config.engine}://{mysql_config.user}:{mysql_config.password}@"
    f"{mysql_config.host}:{mysql_config.port}/{mysql_config.database}"
)

##################################################################
# Cec settings
##################################################################
# 健康度接收SYSOM_HEALTH_METRIC格式异常指标主题
CEC_TOPIC_SYSOM_HEALTH_METRIC = "SYSOM_HEALTH_METRIC"

##################################################################
# gcache settings
##################################################################
CLUSTER_HEALTH_METRIC_GCACHE = "cluster_health_metrics"
NODE_HEALTH_METRIC_GCACHE = "node_health_metrics"
POD_HEALTH_METRIC_GCACHE = "pod_health_metrics"
CLUSTER_METRIC_EXPORTER = "cluster_metric_exporter"
NODE_METRIC_EXPORTER = "node_metric_exporter"
POD_METRIC_EXPORTER = "pod_metric_exporter"