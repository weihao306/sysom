# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                crud.py
Description:
"""
from typing import Optional
from sqlalchemy.orm import Session
from app import models, schemas

################################################################################################
# Define database crud here
################################################################################################

def create_knowledge(db: Session, Knowledge: schemas.Knowledge) -> models.Knowledge:
    knowledge_item = models.Knowledge(**Knowledge.dict())
    db.add(knowledge_item)
    db.commit()
    db.refresh(knowledge_item)
    return knowledge_item

def get_knowledge_by_calltrace(db: Session, calltrace_extrack: str) -> Optional[models.Knowledge]:
    return db.query(models.Knowledge).filter(models.Knowledge.calltrace_extrack == calltrace_extrack).all()

def create_knowledge_record(db: Session, KnowledgeRecord: schemas.KnowledgeRecord) -> models.KnowledgeRecord:
    knowledge_record_item = models.KnowledgeRecord(**KnowledgeRecord.dict())
    db.add(knowledge_record_item)
    db.commit()
    db.refresh(knowledge_record_item)
    return knowledge_record_item

def get_knowledge_by_calltrace(db: Session, calltrace: str) -> Optional[models.Knowledge]:
    return db.query(models.Knowledge).filter(models.Knowledge.calltrace == calltrace).all()

def get_knowledge_by_funcs(db: Session, funcs: str) -> Optional[models.Knowledge]:
    return db.query(models.Knowledge).filter(models.Knowledge.funcs == funcs).all()

def get_knowledge_by_logs(db: Session, logs: str) -> Optional[models.Knowledge]:
    return db.query(models.Knowledge).filter(models.Knowledge.logs == logs).all()

