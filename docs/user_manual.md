# SysOM 用户手册
## 概述
本文档介绍了SysOM的产品功能，为用户提供SysOM的使用教程，以及在使用过程中常见的故障处理方法。
## 阅读对象
该文档的适用对象包括：

- 操作系统及基础设施运维人员
- 操作系统迁移管理人员
## 安装SysOM工具
### 环境要求
操作系统要求
centos7及以上，anolis os 8.4及以上
硬件要求
CPU数建议8核以上，内存建议32G以上。如果需要热补丁功能，CPU数建议32核以上。
SysOM 2.1及之前的版本，只支持x86_64架构。
软件包要求
支持python3及python3相关软件包，grafana及grafana依赖包，make/gcc等编译软件包及相关依赖包，mysql/mariadb，redis，nfs-utils，rpcbind，wget，ssh，nginx，supervisord，yum，dnf等。
### 操作步骤

- 下载RPM包
```
wget https://gitee.com/anolis/sysom/releases/download/v2.0/sysom-2.0-1.an8.x86_64.rpm
```

- 安装 rpm 包
```
rpm -ivh sysom-2.0-1.an8.x86_64.rpm
# 或 yum install -y sysom-2.0-1.an8.x86_64.rpm
```

   - 默认安装路径为 /usr/local/sysom 下
   - 默认配置使用的nginx对外端口为80，可以通过 export SERVER_PORT=xxx 来设置
   - 默认配置的内网IP是通过 ip -4 route 命令查找的第一个IP，可以通过 export SERVER_LOCAL_IP=xxx.xxx.xxx.xxx 来设置
- 启动
```
# 使用以下命令进行启动:
bash -x /usr/local/sysom/init_scripts/server/init.sh
```

- 当服务日志输出下列日志表示部署成功：
```
Oct 10 12:58:51 mfeng bash[3217754]: /usr/local/sysom/init_scripts/server
Oct 10 12:58:51 mfeng bash[3217754]: + for dir in `ls`
Oct 10 12:58:51 mfeng bash[3217754]: + '[' -d init.sh ']'
Oct 10 12:58:51 mfeng bash[3217754]: + for dir in `ls`
Oct 10 12:58:51 mfeng bash[3217754]: + '[' -d stop.sh ']'
Oct 10 12:58:51 mfeng bash[3217754]: + sed -i 's/^FIRST_INIT_DONE=0/FIRST_INIT_DONE=1/g'     /usr/local/sysom/init_scripts/server/init.sh
```
## 访问和登陆
### 浏览器要求
建议Safari 版本15.6.1 ，Chrome 版本109.0.5414.119，Microsoft Edge 版本110.0.1587.50等以上
### 操作步骤
打开本地PC机的浏览器，在地址栏输入http://部署服务器的ip（例如：[http://172.22.3.238](http://172.22.3.238)），按“Enter”。

> **说明：** HTTP默认端口为80，请确认防火墙已开通80端口。

打开登陆界面，如图所示。默认的用户名密码：admin/123456。
![截屏2023-02-06 18.03.01.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/84856280/1675677791282-a8cf9751-7083-403f-ac00-0447eb770a88.png#clientId=u64550777-cb45-4&from=drop&id=u93a0bb5c&name=%E6%88%AA%E5%B1%8F2023-02-06%2018.03.01.png&originHeight=915&originWidth=1918&originalType=binary&ratio=1&rotation=0&showTitle=false&size=113232&status=done&style=none&taskId=uca37021d-a671-4597-9482-f73d144096e&title=)
#### 修改密码
为了您的系统安全，建议请立即修改admin的密码。
点击账户密码修改，输入用户名、原始密码和新密码并确认。点击修改密码，密码修改完成。
参数说明如下表所示。

| **参数** | **说明**                                                                                                                                                  |
| -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 密码     | 设置登陆用户密码需要满足以下要求：<br/>SysOM 2.1版本之前没要求。<br/>SysOM 2.1之后的版本要求：密码不能少于8位且必须包含英文字母、数字、特殊字符三种及以上 |


## 主机管理
### 概述
作为SysOM管控界面，承担管理操作系统的集群增删、主机增删、终端操作系统等功能。你可以通过简单的界面操作系统将需要管理的机器添加进管理界面，也可以一键登录到对应的机器进行shell操作系统。
### 创建集群/批量导入集群
#### 操作步骤

1. 进入“主机管理”菜单，下拉菜单中选择点击”集群列表“进入“集群管理”页面。
2. 点击“新建集群”打开新建集群弹出框，输入以下参数。

| **参数** | **说明**                                                |
| -------- | ------------------------------------------------------- |
| 集群名称 | 设置集群名称需要满足以下要求：集群名称长度应小于128字节 |
| 备注信息 | 输入集群的备注信息。                                    |

3. 点击“确认”，完成集群创建。
4. 点击“批量导入”弹出“批量导入”弹出框，点击“模板下载”将模板下载到本地，填入步骤2中的参数并保存文件。点击“单击上传”，选中保存的文件，点击“确认”完成集群批量导入。
### 删除集群/批量删除集群
#### 操作步骤

1. 在集群列表中找到想要删除的集群，点击“操作”列的“删除”弹出确认删除提示框，点击“OK”即可将该集群信息删除。
2. 批量删除集群，选中需要删除的集群的复选框，或者选中“集群名称”前的复选框选中所有集群。点击列表右上角的“批量删除”，完成集群的批量删除。

> **说明：** 包含主机的集群不允许被删除，若想删除则需要将主机删除或移出集群。
 

### 导出集群
#### 操作步骤
选中需要导出的集群的复选框，或者选中“集群名称”前的复选框选中所有集群。点击列表右上角的“导出数据”，完成集群数据的导出。
### 导入主机\批量导入主机
#### 前提条件
已成功创建集群。
#### 操作步骤

1. 进入“主机管理”菜单，下拉菜单中选择点击”主机列表“进入“主机管理”页面。
2. 单击“新建主机”打开新建主机弹出框，输入以下参数。

| **参数** | **说明**                                                                         |
| -------- | -------------------------------------------------------------------------------- |
| 选择集群 | 选择已创建集群。                                                                 |
| 主机名称 | 主机名称为该主机在SysOM平台上的名称，需要满足以下要求：主机名称长度应小于100字节 |
| 用户名称 | 输入被管理主机的用户的名称。                                                     |
| 用户密码 | 输入被管理主机的用户的密码。                                                     |
| IP地址   | 输入被管理主机的IP地址。                                                         |
| 端口     | 输入被管理主机的SSH协议端口。                                                    |
| 备注信息 | 输入备注信息。                                                                   |

> **说明：** 被管理主机与部署SysOM工具的环境需要保持可联通状态。

3. 点击“确认”，完成主机导入。

> **说明：** 当主机列表中“主机状态”为“运行中”时，主机导入成功；若为“异常”或“离线”，则导入失败。请检查主机信息、网络连通性及主机状态。


4. 点击“批量导入”弹出“批量导入”弹出框，点击“模板下载”将模板下载到本地，填入步骤2中的参数并保存文件。点击“单击上传”，选中保存的文件，点击“确认”完成主机批量导入。

![截屏2023-02-08 15.35.05.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/84856280/1675841713939-9388b836-85fb-4b80-9823-5479cea04568.png#clientId=ud04b7c0a-f993-4&from=drop&id=u2a1dae75&name=%E6%88%AA%E5%B1%8F2023-02-08%2015.35.05.png&originHeight=1496&originWidth=3834&originalType=binary&ratio=1&rotation=0&showTitle=false&size=392391&status=done&style=none&taskId=u88977400-a1e1-4d78-a753-2e0dd36cf16&title=)
### 删除主机/批量删除主机
#### 操作步骤

1. 在集群列表中找到想要删除的主机，点击“操作”列的“删除”弹出确认删除提示框，点击“OK”即可将该主机删除。
2. 批量删除主机，选中需要删除的主机的复选框，或者选中“集群名称”前的复选框选中所有主机。点击列表右上角的“批量删除”，完成主机的批量删除。
### 导出主机
#### 操作步骤
选中需要导出的主机的复选框，或者选中“集群名称”前的复选框选中所有主机。点击列表右上角的“导出数据”，完成主机数据的导出。
### 编辑主机
#### 操作步骤
在主机列表中找到想要删除的主机，点击“操作”列的“编辑”弹出编辑主机框，修改相应主机信息，点击“确认”即可完成主机信息的编辑。

> **说明：** 目前仅支持“选择集群”和“备注信息”的编辑。

### 访问主机
#### 前提条件
被访问的主机的“主机状态”为“运行中”。
#### 操作步骤
在主机列表中找到想要访问的主机，点击“操作”列的“终端”，在右侧打开该主机的在线终端，即可在页面访问主机并输入命令。
## 操作系统迁移
### 概述
操作系统迁移为用户提供了一个简单可视化的界面，来完成一站式的迁移工作，核心功能包含迁移前的评估工作及操作系统迁移实施。
平台支持操作迁移的操作系统列表如下。

| **源操作系统** | **目的操作系统** |
| -------------- | ---------------- |
| CentOS 7.6     | Anolis OS 8      |

### 开始迁移评估
#### 前提条件
主机列表的主机的“主机状态”为“运行中”。
迁移评估的主机操作系统在迁移支持的操作系统列表中。
迁移评估的主机没有正在运行的迁移评估任务。
迁移评估的主机当前没有执行迁移实施。
#### 操作步骤

1. 进入“操作系统迁移”菜单，下拉菜单中选择点击”迁移评估“进入“迁移评估”页面。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677137690351-5e340c1c-1c30-43e4-a6a6-5c9db7807250.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=303&id=u9aea09ab&name=image.png&originHeight=606&originWidth=3538&originalType=binary&ratio=2&rotation=0&showTitle=false&size=422124&status=done&style=none&taskId=u12d560b8-7350-430c-b0ec-debfc10ad0d&title=&width=1769)

2. 填写评估所需的信息，参数说明如下。

| **参数** | **说明**                                                                                                                                                                                     |
| -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 选择机器 | 选择需要被评估的机器，可多选。                                                                                                                                                               |
| 迁移版本 | 选择机器迁移的目的操作系统版本。                                                                                                                                                             |
| Repo配置 | 用于配置迁移评估的仓库 (其中仓库代指 yum 源软件仓库)，此处可选择使用公网地址的默认地址或内网地址。在评估阶段，SysOM 会自动分析仓库的软件包和基础库，将其与本地环境做比对并得出一封评估报告。 |
| 选择评估 | 勾选本次评估的具体内容，其中风险评估为必选项，系统评估，硬件评估，应用评估为可选项。                                                                                                         |

> **说明：** </br>
>
> Repo配置：
>   - 若选择公网地址，则 SysOM 会尝试从社区源 ([https://mirrors.openanolis.cn/)](https://mirrors.openanolis.cn/)) 取回数据，届时请确保主机可以访问公网；
>   - 若选择内网地址，则需要在内网搭建软件仓库，具体搭建方法可以参考社区知识库中的帮助信息([https://openanolis.cn/sig/migration/doc/447499505912234337](https://openanolis.cn/sig/migration/doc/447499505912234337))。
>
> 选择评估：
>   - 勾选应用评估之后，请在弹出框中输入需要评估的应用列表，用英文逗号分隔，支持模糊搜索，具体可评估的应用列表请在被评估主机上执行 rpm -qa 进行查看。 

3. 点击“开始评估”，生成一条评估记录，可通过“评估进度”列查看主机迁移评估的进度。“评估进度”和“评估说明”的说明如下。

| **参数** | **数值** | **说明**                                                       |
| -------- | -------- | -------------------------------------------------------------- |
| 评估进度 | <100%    | 代表当前正在进行评估。                                         |
|          | 100%     | 代表评估已完成。                                               |
| 评估状态 | 评估中   | 代表当前正在进行评估。                                         |
|          | 评估停止 | 代表正在评估中的主机在“操作”中被执行了“停止”操作，评估未完成。 |
|          | 评估失败 | 代表评估失败。                                                 |
|          | 评估完成 | 代表评估已完成。                                               |

> **说明：** 
> 根据勾选评估内容依次执行评估，入遇到某个评估失败，则当前整个评估任务失败，评估失败原因可通过鼠标悬浮在“评估失败”状态上查看。 

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677140769711-2e6550c8-72e8-4637-89cb-7fbbc1066a9d.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=280&id=u60dd292c&name=image.png&originHeight=560&originWidth=3506&originalType=binary&ratio=2&rotation=0&showTitle=false&size=434599&status=done&style=none&taskId=u24c91f6a-097f-4281-97cf-a7524abd459&title=&width=1753)
### 停止评估
#### 操作步骤
在评估记录列表中找到想要停止评估的主机，点击“操作”列的“停止”，即可停止该主机的评估任务。
### 重试评估
#### 操作步骤
在评估记录列表中找到想要重新评估的主机，点击“操作”列的“重试”，即可新开始该主机的评估并生成一条评估记录。
### 删除评估记录
#### 操作步骤
在评估记录列表中找到想要删除的评估记录，点击“操作”列的“删除”，即可删除该条评估记录。
### 查看迁移报告
#### 操作步骤

1. 在评估记录列表中找到想要查看的主机，点击“操作”列的“查看报告”跳转到评估报告详情页面，点击不同的评估项可切换到不同的报告内容，即使未评估完成或者评估失败，也可以查看某一项已经评估完成的内容。
2. 报告说明如下表。

| **评估报告** | **参数** | **说明**                                                                                   |
| ------------ | -------- | ------------------------------------------------------------------------------------------ |
| 风险评估     | 风险项   | 迁移过程中可能出现的风险问题                                                               |
|              | 风险     | 风险等级                                                                                   |
|              | 建议操作 | 可按照该操作处理当前风险                                                                   |
|              | 评估结果 | 如果不存在阻塞迁移的风险，则评估通过，如果存在阻塞迁移的风险，则需要用户手动介入处理风险。 |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677139282459-008aac50-ca9f-40a9-9401-c501cbb0e5ca.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=867&id=uf2905d1b&name=image.png&originHeight=1734&originWidth=2614&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1101676&status=done&style=none&taskId=uce88f901-a0e3-41d4-baac-535fcefe0bd&title=&width=1307)

| **评估报告** | **参数**     | **说明**                     |
| ------------ | ------------ | ---------------------------- |
| 系统评估     | 环境变量     | 展示系统环境变量及其评估结果 |
|              | 系统服务     | 展示系统服务状态及其评估结果 |
|              | 系统命令     | 展示系统命令列表及其评估结果 |
|              | 内核模块     | 展示内核模块列表及其评估结果 |
|              | 系统调用     | 展示系统调用列表及其评估结果 |
|              | 内核启动参数 | 展示内核启动参数及其评估结果 |
|              | 内核动态配置 | 展示内核动态参数及其评估结果 |
|              | 内核静态配置 | 展示内核静态参数及其评估结果 |
|              | KABI         | 展示KABI接口列表及其评估结果 |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677140462324-a21f13e2-4d0b-4252-a27f-ef537a40a90f.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=870&id=ubea93b01&name=image.png&originHeight=1740&originWidth=2600&originalType=binary&ratio=2&rotation=0&showTitle=false&size=947414&status=done&style=none&taskId=uab889829-1730-405a-9db3-f2b3f3f5694&title=&width=1300)

| **评估报告** | **参数**                  | **说明**                                                               |
| ------------ | ------------------------- | ---------------------------------------------------------------------- |
| 硬件评估     | 整机信息                  | 被评估主机的一些基本物理硬件信息                                       |
|              | 设备名称                  | 主机包含的板卡的设备名称                                               |
|              | bdf、vid、did、svid、sdid | 板卡硬件各类ID信息                                                     |
|              | 驱动评估结果              | 基于驱动信息评估的结果，支持表示存在支持该板卡的驱动                   |
|              | ancert验证结果            | 基于社区ancert硬件兼容性工具验证的结果，通过表示经过测试验证支持该板卡 |
|              | 操作                      | 点击可跳转到龙蜥兼容适配网站进行自主验证                               |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677140199006-0c82bc58-8ba5-44d1-89f4-9b6a675881ab.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=848&id=ue103cf41&name=image.png&originHeight=1696&originWidth=2602&originalType=binary&ratio=2&rotation=0&showTitle=false&size=985062&status=done&style=none&taskId=ub3915af4-ff7f-4122-b502-cb615cb569d&title=&width=1301)

| **评估报告** | **参数**     | **说明**                                      |
| ------------ | ------------ | --------------------------------------------- |
| 应用评估     | 应用列表     | 被评估应用的列表                              |
|              | 操作         | 点击详细可跳转到该应用具体的评估内容          |
|              | 依赖评估结果 | 展示应用最终评估结果                          |
|              | 应用依赖评估 | 展示当前应用依赖的lib库、系统命令及其评估结果 |
|              | ABI评估报告  | 展示应用依赖的lib库ABI接口及其评估结果        |
|              | CLI评估报告  | 展示应用依赖的CLI接口及其评估结果             |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677141002003-4f1522d5-7aba-495d-bb81-c4446f95e04e.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=869&id=ud61a8f0b&name=image.png&originHeight=1738&originWidth=2596&originalType=binary&ratio=2&rotation=0&showTitle=false&size=847372&status=done&style=none&taskId=u5c81c641-7555-47c7-ab0d-b826dd743ac&title=&width=1298)
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677141073667-5441f06d-a82d-47d7-98dc-a8e6ee022b90.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=896&id=u27d8f4a0&name=image.png&originHeight=1792&originWidth=2614&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1133265&status=done&style=none&taskId=u5f9a19b8-e631-4d60-b79c-ee19057a1fb&title=&width=1307)
### 迁移实施
#### 前提条件
主机列表的主机的“主机状态”为“运行中”。
迁移实施的主机操作系统在迁移支持的操作系统列表中。
迁移实施的主机没有正在运行的迁移评估任务。
迁移实施的主机当前没有执行迁移实施。
#### 操作步骤

1. 进入“操作系统迁移”菜单，下拉菜单中选择点击”迁移实施“进入“迁移实施”页面。
2. 菜单机器列表中可查看所有主机及状态，点击左上角可切换集群，找到需要迁移的主机，在操作列点击“...”弹出下拉菜单。点击“迁移配置”弹出迁移配置框。参数说明如下。

| **参数** | **说明**                                                                                                                                                                                 |
| -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 选择机器 | 显示所选机器。                                                                                                                                                                           |
| 迁移版本 | 选择机器迁移的目的操作系统版本。                                                                                                                                                         |
| 选择内核 | 选择内核，目前仅支持迁移至ANCK内核。                                                                                                                                                     |
| Repo配置 | 用于配置迁移实施的仓库 (其中仓库代指 yum 源软件仓库)，此处可选择使用公网地址的默认地址或内网地址。在迁移实施阶段，SysOM 会自动从仓库中下载所需的软件包，用以替换当前系统的基础软件体系。 |
| 备份配置 | 选择是否在迁移前进行备份，若不需要备份选择“不备份”，需要则选择“NFS备份”并填入NFS服务的IP地址、输入NFS的目录名称及该主机无需备份的目录名称。                                              |

> **说明：**</br>
> 
> Repo配置
>   - 若选择公网地址，则 SysOM 会尝试从社区源 ([https://mirrors.openanolis.cn/)](https://mirrors.openanolis.cn/))取回数据，届时请确保主机可以访问公网；
>   - 若选择内网地址，则需要在内网搭建软件仓库，具体搭建方法可以参考社区知识库中的帮助信息([https://openanolis.cn/sig/migration/doc/447499505912234337](https://openanolis.cn/sig/migration/doc/447499505912234337))。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677144084945-d49151d9-6d29-4309-b9fd-4a9e7230bef0.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=660&id=u4ace76d5&name=image.png&originHeight=1320&originWidth=1928&originalType=binary&ratio=2&rotation=0&showTitle=false&size=515079&status=done&style=none&taskId=u7ef0a5ba-15e8-4efb-be2a-798dd794d5d&title=&width=964)

1. 点击“确定”，主机“迁移状态”变为“就绪中”。根据下表的步骤操作，当迁移状态为“就绪中”时，表示本次操作已完成，可以点击操作列“...”进行下一步操作，直至迁移完成，如果迁移状态为“失败”，失败原因可通过鼠标悬浮在迁移状态上查看。迁移中的所有操作步骤及说明如下。

| **步骤** | **说明**                                                                                               |
| -------- | ------------------------------------------------------------------------------------------------------ |
| 迁移配置 | 配置迁移主机的迁移参数，详情参考步骤2。                                                                |
| 系统备份 | 根据步骤2中的备份选择，如果选择NFS备份，则该步骤会对迁移主机进行备份。                                 |
| 环境准备 | 对迁移主机下发迁移工具，并安装部署。                                                                   |
| 风险评估 | 使用迁移工具进行风险评估，评估当前机器迁移的风险。                                                     |
| 迁移实施 | 使用迁移工具进行迁移实施，将当前机器迁移到指定版本。                                                   |
| 重启机器 | 迁移实施完成后，需要重启机器进行系统切换，重启之后才算本次迁移完成。                                   |
| 系统还原 | 如果原系统进行了备份，那么可以在当前机器的任意状态进行系统还原，即使已经完成迁移，也可以进行系统还原。 |
| 重置状态 | 如果发现配置错误或者想修改配置，则可以使用重置状态，重置当前主机的迁移状态。                           |

### 批量迁移实施
#### 操作步骤

1. 进入“操作系统迁移”菜单，下拉菜单中选择点击”迁移实施“进入“迁移实施”页面。
2. 点击左侧“批量实施”按钮，弹出参数配置框，在选择机器里可以选择多台机器并行迁移，其它参数含义与迁移实施参数一致。
3. 批量实施会针对所选择的机器，按照迁移配置-系统备份-环境准备-风险评估-迁移实施-重启机器等步骤依次自动执行，无需人工介入即可完成所有迁移。
4. 如果部分机器遇到问题，则会自动停止后续迁移步骤，需要人工手动介入，然后再次执行完成迁移。
### 查看迁移主机机器信息
#### 操作步骤
在迁移实施页面左侧点击需要查看的主机，然后在右侧点击机器信息，即可查看该主机的一些基本信息，硬件信息和软件信息。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677145354865-df9baa24-366e-4baf-ac06-a4e752e8e232.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=848&id=u464f09f7&name=image.png&originHeight=1696&originWidth=2240&originalType=binary&ratio=2&rotation=0&showTitle=false&size=781362&status=done&style=none&taskId=u2784f07c-0762-4f21-9338-f98381a3700&title=&width=1120)
### 查看迁移主机迁移信息
#### 操作步骤
在迁移实施页面左侧点击需要查看的主机，然后在右侧点击迁移信息，即可查看该主机的迁移配置和当前迁移步骤完成情况。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677145447052-0f0a86a8-dbbd-49fc-8b0a-b00efbb98a8d.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=667&id=u38b9bcb6&name=image.png&originHeight=1334&originWidth=2246&originalType=binary&ratio=2&rotation=0&showTitle=false&size=607151&status=done&style=none&taskId=ud29c798a-e90d-4a64-899c-f73b6856865&title=&width=1123)
### 查看迁移主机迁移日志和报告
#### 操作步骤
在迁移实施页面左侧点击需要查看的主机，然后在右侧点击迁评估日志，评估报告，实施日志，实施报告，即可查看该主机在评估和迁移阶段产生的实时日志和结果报告。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/28856758/1677145569683-15ae7ae9-154e-4abb-bb1c-547801fde8fb.png#clientId=u1ca12cdf-d0b7-4&from=paste&height=884&id=uf7422773&name=image.png&originHeight=1768&originWidth=2240&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1022908&status=done&style=none&taskId=uc4038d13-a6a8-488b-8224-58b2855e51d&title=&width=1120)
## 监控中心
### 概述
监控中心主要为用户全方位的展示操作系统情况，包括但不限于：资源使用、任务负载、系统任务。
### 迁移监控
#### 前提条件
已成功添加一台或多台主机，主机登陆权限需要是root权限。
对一台Centos7的机器实施操作系统迁移操作，迁移到龙蜥操作系统
#### 操作步骤

1. 进入“监控中心”菜单，下拉菜单中选择点击”迁移监控“进入“迁移监控”页面。
2. 在左侧“机器列表”中点击需要查看的主机，右侧的监控面板，就会显示该主机的迁移监控各个指标。指标项简要说明：

| **指标栏** | 指标项                    | **说明**                                                                                                                             |
| ---------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| 资源总额   | 原始/当前内核版本         | 指标项主要展示主机的一些主要资源的总体情况，包括：内核版本，内存总量，大页内存总量，磁盘空间，磁盘个数，网卡数量，启用的网卡数量等。 |
|            | 原始/当前内存可用总量     |                                                                                                                                      |
|            | 原始/当前大页内存总量     |                                                                                                                                      |
|            | 原始/当前磁盘可用空间总量 |                                                                                                                                      |
|            | 原始/当前磁盘个数         |                                                                                                                                      |
|            | 原始/当前网卡数量         |                                                                                                                                      |
|            | 原始/当前启用网卡数量     |                                                                                                                                      |
| 资源监控   | CPU                       | CPU个数，CPU当前的利用率，CPU两日的利用率，以及CPU利用率两日的波动情况。                                                             |
|            | Memory                    | Memory使用率，以及内存使用率波动情况                                                                                                 |
|            | Disk                      | 磁盘空间总额，使用率，磁盘的IOPS                                                                                                     |
|            | Network                   | 网络流量监控，网络流量的波动情况                                                                                                     |
|            | System Load               | 系统负载值，以及系统负载的波动情况                                                                                                   |

> 原始值：代表主机执行操作系统迁移前的情况。如果没有执行过操作系统迁移，那原始的值都为空（No data）         
> 
> 当前值：代表主机当前的资源情况 

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676947587774-a7738ddd-4745-426d-aa52-aaf69ae64c91.png#clientId=uff2e0d99-6f01-4&from=paste&height=383&id=u94887f63&name=image.png&originHeight=626&originWidth=1247&originalType=binary&ratio=2&rotation=0&showTitle=false&size=119110&status=done&style=none&taskId=u2603d45f-242d-416b-b54c-bbec669f56c&title=&width=762)
### 系统监控
#### 前提条件
已成功添加一台或多台主机，主机登陆权限需要是root权限。
#### 操作步骤

1. 进入“监控中心”菜单，下拉菜单中选择点击”系统监控“进入“系统监控”页面。
2. 在左侧“机器列表”中点击需要查看的主机，右侧的监控面板，就会显示该主机的系统监控各个指标。

指标项简要说明(指标项太多，这里不会全列出来)：

| **指标栏**                                                                                      | 指标项                | **说明**                                                               |
| ----------------------------------------------------------------------------------------------- | --------------------- | ---------------------------------------------------------------------- |
| Quick CPU/Mem/Disk<br/>(主要展示一个整机的大体资源使用情况，以及CPU/Mem/Disk等资源的总额情况。) | CPU Busy              | CPU Busy：整机的CPU使用率                                              |
|                                                                                                 | Sys Load              | 整机的负载                                                             |
|                                                                                                 | RAM Used              | 整机的内存使用率                                                       |
|                                                                                                 | Swap Used             |                                                                        |
|                                                                                                 | Root FS Used          | 根文件系统使用率                                                       |
|                                                                                                 | CPU cores             | CPU个数                                                                |
|                                                                                                 | Uptime                | 机器运行时长                                                           |
| Basic CPU / Mem / Net / Disk                                                                    | CPU Basic             | CPU几种状态的占用率情况（System,User,IOwait,IRQS,idle,Other）          |
|                                                                                                 | Memory Basic          | Memory使用率基本情况（Total,Used,Cache,free,swap）                     |
|                                                                                                 | Network Traffic Basic | 网络流量收发情况                                                       |
|                                                                                                 | Disk Space Used Basic | 磁盘使用率情况                                                         |
| 其它                                                                                            | 省略                  | 其它的监控指标主要是根据CPU/Mem/Net/Disk一个个深入展开的具体的指标项。 |


![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676947044123-058f35f3-50a7-4894-88a2-3527fc06b4f1.png#clientId=u097c80c9-81cf-4&from=paste&height=605&id=ubc3b4707&name=image.png&originHeight=1210&originWidth=2830&originalType=binary&ratio=2&rotation=0&showTitle=false&size=2365563&status=done&style=none&taskId=uf3c4ae15-bbb9-43fd-ab26-30fc5204beb&title=&width=1415)
## 宕机中心
### 概述
宕机问题作为操作系统异常的一种极端情况，往往会对用户的应用产生重大影响。SysOM宕机中心将会为用户提供统一的宕机问题管理，包括宕机监控、已知问题管理、在线分析、解决方案等能力。通过多维度的宕机管理，降低宕机问题对用户的影响，提升用户业务的稳定性。
### 宕机列表
#### 前提条件
已成功添加一台或多台主机，主机登陆权限需要是root权限。
节点端主机有宕机场景，否则SysOM服务端收集到的宕机数据为空。
#### 操作步骤

1. 进入“宕机中心”菜单，下拉菜单中选择点击”宕机列表“进入“宕机列表”页面。
2. 在左侧“机器列表”中点击需要查看的主机，右侧的监控面板，就会显示该主机的系统监控各个指标。

指标说明：

| **指标栏** | 指标项                                | **说明**                               |
| ---------- | ------------------------------------- | -------------------------------------- |
| 核心指标   | 最近30天的宕机                        | 最近30天的宕机总数                     |
|            | 最近7天宕机总数                       | 最近7天宕机总数                        |
|            | 月宕机率                              | 一个月内宕机的主机数 / 主机列表机器数  |
|            | 日宕机率（后续修正为“最近7天宕机率”） | 最近7天的宕机的主机数 / 主机列表机器数 |
| 宕机列表   | 主机名称                              | 发生宕机的主机名称                     |
|            | IP                                    | 发生宕机的主机IP                       |
|            | 宕机时间                              | 发生宕机的时间                         |
|            | 内核版本                              | 发生宕机的主机的内核版本               |
|            | Vmcore                                | 是否有vmcore产生                       |
|            | 解决方案                              | 是否有对应的解决方案                   |
|            | 宕机详情                              | 跳转查看宕机的详细信息                 |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677135469600-b0b59788-37b0-4cc6-8162-1611b2505ea0.png#clientId=u9bab3d2c-0e11-4&from=paste&height=318&id=ua4c89388&name=image.png&originHeight=1208&originWidth=2818&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1615561&status=done&style=none&taskId=ue7480e9c-8434-41dd-83e9-e032fc10a25&title=&width=742)

3. 在“宕机列表”的“宕机详情”列，点击“查看”按钮，跳转到宕机详情页面。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677143173390-47075dc1-564d-4b9c-a2e6-591b7dcaa250.png#clientId=u9bab3d2c-0e11-4&from=paste&height=589&id=u4c0b1282&name=image.png&originHeight=1178&originWidth=2802&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1673684&status=done&style=none&taskId=ub1d7d376-8a0c-431f-874a-972962ee978&title=&width=1401)

4. 点击“录入解决方案”，会弹窗，接受用户输入解决方案

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677143478728-d55c58f8-6a59-45f5-94ce-0947f82a897f.png#clientId=u9bab3d2c-0e11-4&from=paste&height=292&id=u4f0d03ea&name=image.png&originHeight=584&originWidth=1610&originalType=binary&ratio=2&rotation=0&showTitle=false&size=441542&status=done&style=none&taskId=u110d158c-c0f7-42f3-af64-568cad67421&title=&width=805)

5. 输入“解决方案”，点击“确认”，会回跳到宕机详情页面

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677144716132-c3624892-262b-40cf-8e8d-c2ce401b497d.png#clientId=u9bab3d2c-0e11-4&from=paste&height=375&id=u33f0401a&name=image.png&originHeight=750&originWidth=2776&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1010723&status=done&style=none&taskId=u6a8e30c0-b69e-41da-a053-c243934a5a6&title=&width=1388)

6. “宕机详情”页面的最下面，有宕机调用栈匹配结果，自动匹配调用栈，以及解决方案。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677144644056-484f2c01-9f51-44a8-9f5b-7f0e86d51dd2.png#clientId=u9bab3d2c-0e11-4&from=paste&height=524&id=uf57472f2&name=image.png&originHeight=1048&originWidth=2794&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1359082&status=done&style=none&taskId=ud26d0668-73c5-4cbc-b319-84260d01ba9&title=&width=1397)
### 宕机匹配
#### 前提条件
宕机列表列表不为空
#### 操作步骤

1. 进入“宕机中心”菜单，下拉菜单中选择点击”宕机匹配“进入“宕机匹配”页面。
2. 在“相似调用栈”的输入框中输入宕机调用栈，然后点击“查询”按钮。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677145023998-3c03218e-47c6-4933-9334-146bd1caedb7.png#clientId=u9bab3d2c-0e11-4&from=paste&height=528&id=u55cb0257&name=image.png&originHeight=1056&originWidth=2782&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1481246&status=done&style=none&taskId=ua2517f9d-6dd7-407b-ae23-6d9b671dda4&title=&width=1391)

3. 点击“查询”按钮后，在下端的“宕机列表”中，就能知道，当前调用栈，在已有的“宕机列表”中，是否有相似的调用栈，以及调用栈相似度。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1677145151624-e3bb20d8-8307-46e6-9a9a-2c732ca0c939.png#clientId=u9bab3d2c-0e11-4&from=paste&height=581&id=u51a1736b&name=image.png&originHeight=1162&originWidth=2746&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1631321&status=done&style=none&taskId=ue27f6b61-d2ca-4309-9402-cd48c8182bf&title=&width=1373)

4. 用户可以根据“相似度”，点击“查看”按钮，可以跳转到对应的“宕机详情”页，查看解决方案。
### 宕机配置
#### 功能待开发
## 诊断中心
### 概述
操作系统作为非常底层的软件基础设施，运维难度较高，针对操作系统层面的问题，SysOM为用户提供了全方位的诊断能力，包括：系统健康检查(即一键诊断)、网络/内存/存储/调度深度诊断，通过全方位诊断帮助用户分析操作系统层面的各类问题。
### 系统健康检查
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机
- 主机登录权限需要是root权限（执行 sysak 需要）
#### 功能描述
系统健康检查功能提供一键式系统健康评估，对系统的配置、SLI、已知问题、日志和硬件进行检查，并详细罗列每一个检查项的检查状态。
#### 操作步骤
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677122769222-f281a516-073a-4066-b068-6638eb6bfc54.png#clientId=ufa81fab2-48fd-4&from=paste&height=825&id=uca47282f&name=image.png&originHeight=1650&originWidth=3042&originalType=binary&ratio=2&rotation=0&showTitle=false&size=586466&status=done&style=none&taskId=ueda88bd8-de4e-4023-bd75-e6f3958e13d&title=&width=1521)
### 调度诊断中心
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机
- 主机登录权限需要是root权限（执行 sysak 需要）
#### 调度抖动诊断
##### 1. 使用场景
CPU长时间在内核态执行，导致用户态进程长期得不到调度，或者系统长时间关中断，导致CPU无法正常接受 TICK 中断 => 引发调度抖动，导致业务进程突发调度延迟，甚至系统短暂hang。
##### 2. 功能描述
记录调度抖动发生的时间点、发生的次数、和抖动的具体数值，可以帮助用户更好的定位调度抖动的根因。
##### 3. 诊断参数
| **参数名** | **参数说明**                                                                               | **是否必填** |
| ---------- | ------------------------------------------------------------------------------------------ | ------------ |
| 实例IP     | 目标Node节点的IP                                                                           | 必填         |
| 诊断时间   | 持续诊断时长（时间单位：s）                                                                | 默认为 20s   |
| 诊断阈值   | 延迟的阈值（时间单位：ms），超过这个阈值的延迟样本将会被记录，低于阈值的延迟样本将会被忽略 | 默认为 20ms  |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677132668406-40082593-48f8-412d-8f3a-3167abd3fe7b.png#clientId=udea570a5-e4cf-4&from=paste&height=945&id=uc4e69a94&name=image.png&originHeight=1890&originWidth=3802&originalType=binary&ratio=2&rotation=0&showTitle=false&size=595484&status=done&style=none&taskId=uefd45ea4-edb9-419e-8109-fc2c3f6a0f5&title=&width=1901)
**事件总览：** 目前有3类事件：调度延迟、sys延迟和irq延迟。不同的事件由具体的告警与否(及颜色)来分辨。蓝色表示有告警事件发生，绿色表示正常。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138731310-13f56604-a4e8-41eb-ae2b-194117e30bf2.png#clientId=u9cf628e7-b5c5-4&from=paste&height=231&id=uf6edb7a1&name=image.png&originHeight=462&originWidth=2102&originalType=binary&ratio=2&rotation=0&showTitle=false&size=43041&status=done&style=none&taskId=u71070adc-701c-4d4e-934b-17ff9de8730&title=&width=1051)
**抖动时间线图：** 记录了调度抖动发生的时间点、发生的次数、和抖动的具体数值。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138739739-7dbaa9ef-6f26-4695-8f83-6cd4ef3e04cf.png#clientId=u9cf628e7-b5c5-4&from=paste&height=398&id=ud1a07ad3&name=image.png&originHeight=796&originWidth=2690&originalType=binary&ratio=2&rotation=0&showTitle=false&size=87591&status=done&style=none&taskId=uf8f20f42-d0aa-4b73-8fde-e76ae54acaa&title=&width=1345)
**调度抖动详情：** 将调具体的度抖动事件以table的方式展示出来，下面是表中各列指标说明。

| date                           | class                                                           | latency                  | cpu                     | current                   | stamp                              | extern                                                                                                                        |
| ------------------------------ | --------------------------------------------------------------- | ------------------------ | ----------------------- | ------------------------- | ---------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| 表示调度抖动告警事件发生的日期 | 表示具体的调度抖动事件类型。目前有调度延迟、sys延迟和irq延迟3类 | 具体的抖动延迟值，单位ms | 调度抖动告警发生的cpu号 | 受干扰的当前任务名字和pid | 发生调度抖动告警时的机器上的时间戳 | 额外信息。不同的class有不同的额外信息： </br> 调度延迟：nr_running 表示cpu队列上的排队的任务数量，prev表示当前cpu上前一个任务 |


![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677132864111-201fcc24-7dfe-4473-8f7b-dcd2571c13d3.png#clientId=udea570a5-e4cf-4&from=paste&height=793&id=u65c78130&name=image.png&originHeight=1586&originWidth=3764&originalType=binary&ratio=2&rotation=0&showTitle=false&size=828612&status=done&style=none&taskId=u6d73bb66-2e3a-48b1-8108-5e710bcfd59&title=&width=1882)
##### 5. 离线导入

- 使用场景：当目标机器没有被纳入管理，或者网络不通时，可以在上面安装 sysak ，然后直接执行 sysak 命令进行诊断，然后将诊断结果在前端离线导入就会生成一条诊断记录，可以在前端以图形化的方式展示诊断结果。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677132978973-7f5cb08f-b59f-409a-9507-c94ef2a0b94b.png#clientId=udea570a5-e4cf-4&from=paste&height=942&id=u5862570e&name=image.png&originHeight=1884&originWidth=3836&originalType=binary&ratio=2&rotation=0&showTitle=false&size=634874&status=done&style=none&taskId=u356cfba6-b7a7-4848-baf7-53d1939e1a8&title=&width=1918)
#### 系统负载分析
##### 1. 使用场景
在系统负载较高时，可以使用本诊断功能分析导致系统负载高的原因。
##### 2. 功能描述
SysOM 负载分析诊断功能主要分析系统负载情况，进程分布及对系统的影响。
##### 3. 诊断参数
| **参数名** | **参数说明**     | **是否必填** |
| ---------- | ---------------- | ------------ |
| 实例IP     | 目标Node节点的IP | 必填         |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677127796784-e831ddd6-da5e-49cf-8c9f-4cd296367dc8.png#clientId=ue682e09a-300a-4&from=paste&height=823&id=ub219d906&name=image.png&originHeight=1646&originWidth=3040&originalType=binary&ratio=2&rotation=0&showTitle=false&size=641715&status=done&style=none&taskId=ub183ae63-3ad1-41b6-ad0b-812f0399e26&title=&width=1520)
**事件总览：** 显示当前系统负载，并诊断负载是否对系统sys utils、硬中断、软中断、io有影响。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677127819058-ec41e152-83ea-4df3-8a68-d60ec2a03645.png#clientId=ue682e09a-300a-4&from=paste&height=204&id=u64f7ce5f&name=image.png&originHeight=408&originWidth=2986&originalType=binary&ratio=2&rotation=0&showTitle=false&size=125401&status=done&style=none&taskId=u87b83c07-3109-4830-b0a0-ef0a1654f36&title=&width=1493)
**进程分布：** 诊断当前系统R和D状态的分布情况和导致系统负载高的贡献度。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677127870190-debd0772-4c89-4e28-bc5d-bacc764db3af.png#clientId=ue682e09a-300a-4&from=paste&height=318&id=uad6374bb&name=image.png&originHeight=636&originWidth=2994&originalType=binary&ratio=2&rotation=0&showTitle=false&size=215779&status=done&style=none&taskId=u4167986f-dccf-4c2b-8b89-558a2032f44&title=&width=1497)
**调度火焰图：** 显示系统调度火焰图，可直接查询导致负载高的热点函数。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677127903528-6adb580f-27b5-4572-b152-89c9da74bfdc.png#clientId=ue682e09a-300a-4&from=paste&height=356&id=u6298def1&name=image.png&originHeight=712&originWidth=2990&originalType=binary&ratio=2&rotation=0&showTitle=false&size=286016&status=done&style=none&taskId=ubf75427c-59df-42ec-8c9a-b6e1f762550&title=&width=1495)
##### 5. 离线导入
使用方式同调度抖动诊断
#### 应用profile分析
##### 1.使用场景
##### 2.功能描述
profile系统的热点应用(占用cpu多的任务)，对热点应用给出性能分析，统计应用的用户态与内核态的热点栈占比，最终展示系统top 10的热点应用排布。
##### 3.诊断参数
| **参数名** | **参数说明**     | **是否必填**   |
| ---------- | ---------------- | -------------- |
| 实例IP     | 目标Node节点的IP | 必填           |
| 时间       | 诊断时长         | 选填，默认5min |

##### 4.在线诊断
参数输入
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/21656766/1681367511995-931949b7-e86c-4783-ab04-edfe49cdd061.png#clientId=ue843ffe6-b75d-4&from=paste&height=209&id=uaf049f3d&name=image.png&originHeight=209&originWidth=939&originalType=binary&ratio=1&rotation=0&showTitle=false&size=101197&status=done&style=none&taskId=u8ea91f72-6e37-4923-a0b5-1e54ca6212c&title=&width=939)
诊断结果
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/21656766/1681367883166-cc7319b8-fd7a-4619-9534-dca5adc07017.png#clientId=ue843ffe6-b75d-4&from=paste&height=683&id=u5ca84ecd&name=image.png&originHeight=683&originWidth=1867&originalType=binary&ratio=1&rotation=0&showTitle=false&size=603466&status=done&style=none&taskId=u37333f7d-3140-4102-8634-bca8ba84099&title=&width=1867)

- COMM ：占用系统cpu资源最多的top10任务
- TaskHitRatio：任务占用系统cpu资源的占比
- HotStack：任务详细的热点栈信息
- HotStackHitRatio：应用热点栈占自身所有栈信息的占比
- COMMENT：分析热点栈是在内核还是在应用自身
#### 5.离线诊断
使用方式同调度抖动诊断
### 存储诊断中心
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机
- 主机登录权限需要是root权限（执行 sysak 需要）
#### IO时延分析
##### 1. 使用场景
IO流量积压、存储设备异常等情况容易造成 IO 请求处理慢，IO延迟高。
##### 2. 功能描述
记录存储设备的历史IO延迟水位，并且统计每分钟访问 IO 延迟偏离历史水位的次数，用于快速定位导致 IO 延迟偏高的位置。
##### 3. 诊断参数
| **参数名** | **参数说明**                                   | **是否必填**                 |
| ---------- | ---------------------------------------------- | ---------------------------- |
| 实例IP     | 目标Node节点的IP                               | 必填                         |
| 诊断时长   | 持续诊断时长（时间单位：s）                    | 必填，默认为 10s             |
| 时间阈值   | 保留IO延迟大于设定时间阈值的IO（时间单位：ms） | 必填，默认为 1000ms          |
| 目标磁盘   | 需要检测的磁盘名称，如sda，sdb等。             | 选填（不指定则诊断所有磁盘） |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677134356775-c1ac3cd7-63e8-40a5-baf5-497137406cd3.png#clientId=udea570a5-e4cf-4&from=paste&height=892&id=u9b8b11c0&name=image.png&originHeight=1784&originWidth=3758&originalType=binary&ratio=2&rotation=0&showTitle=false&size=651860&status=done&style=none&taskId=u6c99969e-bd2b-40de-a8dc-4af364e52ed&title=&width=1879)**iolantency 总览：** 表示在一个诊断周期内，捕捉到的超时IO的个数，个数为0时，check result的状态为normal，字体显示绿色，表示当前无异常的慢IO，个数不为0时，check result的状态为abnormal，字体显示红色，表示当前存在超出阈值的慢IO；_注意：阈值在发起诊断的时候可以指定，不指定情况下，默认为1000ms，_在诊断结果最右侧支持磁盘的checklist选择展示不同磁盘的数据，切换数据展示。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677134439288-b5272556-37d9-43c8-895b-b86abebf963d.png#clientId=udea570a5-e4cf-4&from=paste&height=69&id=ubbf5fc0f&name=image.png&originHeight=137&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=106126&status=done&style=none&taskId=ua7dec00d-f6ee-492d-8a25-d7708dad113&title=&width=750)
**整体 IO 时延分布：** 这部分展示IO整体在OS以及磁盘各个存储链路上的延迟分布，每个框图中的指标解读为：

- 左上角为链路名，分别表示IO在经过此路径的延迟损耗，其中主要包括OS、Disk两部分，又根据IO的生命周期，进一步将OS中细分为block、driver、complete三部分（分别表示内核通用块层、驱动、IO完成后回收三部分）
- 右上角表示在此链路耗时占整体耗时的百分比
- 中间的max_us、avg_us、min_us则分别表示IO在此路径上的最大延迟、平均延迟、最小延迟，单位us。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677134489224-0976eb85-a3ed-4e12-a7cc-ad1769fe84b0.png#clientId=udea570a5-e4cf-4&from=paste&height=112&id=u7e07642c&name=image.png&originHeight=223&originWidth=1499&originalType=binary&ratio=2&rotation=0&showTitle=false&size=169868&status=done&style=none&taskId=u288467a1-4305-4c91-a437-4f081eb1aa5&title=&width=749.5)
**单 IO 时延 metrics 展示：** 该部分将捕捉到的每一个超时IO，通过时序数据曲线的方式展示出来，X轴为时间日期，Y轴为延迟(单位us)，图中有5条曲线，分别表示每个IO的total delay、block delay、driver delay、disk delay、complete delay，可以看出这个是更细粒度的针对每一个IO在存储各链路上的延迟分布。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677134537042-2c169564-224e-491a-84c8-43481488fc85.png#clientId=udea570a5-e4cf-4&from=paste&height=112&id=u60be37f1&name=image.png&originHeight=223&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=183726&status=done&style=none&taskId=u7a746161-5078-4096-a72e-1d5452289cc&title=&width=750)
**Top 10 IO 详情：** 这部分展示了IO总延迟大小排在前TOP10的IO的更多细节

- `time`：表示检测到此超时IO的时间日期，精确到ms；
- `abnormal`：表示此IO延迟消耗最大的原因，以“延迟最大的路径（此路径耗时：IO整体耗时 单位）”形式展示；
- `iotype`：表示具体的IO操作，各字母缩写参考：
   - W：Write
   - R：Read
   - S：Sync
   - FWF：分别表示Flush/Write/FUA（force unit acess）
   - M：Metadata
   - D：Discard
- `sector`：表示此IO要访问磁盘的扇区编号；
- `datalen`：表示此IO要访问的数据长度，单位byte；
- `comm`：表示此IO的生产者进程名；
- `pid`：表示此IO的生产者进程id；
- `cpu`：表示此IO在生命周期中的cpu轨迹，即由哪个cpu产生、又有哪个cpu派发、以及磁盘完成此IO由哪个cpu响应的中断。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677134688988-a2daedf8-0f75-4cc6-961c-24250f8c6177.png#clientId=udea570a5-e4cf-4&from=paste&height=89&id=u5d09f268&name=image.png&originHeight=178&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=149889&status=done&style=none&taskId=u933d16a4-7607-4740-a555-3d44f6d36c7&title=&width=750)
##### 5. 离线导入
使用方式同调度抖动诊断。
#### IO流量分析
##### 1. 使用场景
通常用于解决 IO Burst 问题。
##### 2. 功能描述
此功能主要分析系统中的IO流量归属。
##### 3. 诊断参数
| **参数名** | **参数说明**                       | **是否必填**                 |
| ---------- | ---------------------------------- | ---------------------------- |
| 实例IP     | 目标Node节点的IP                   | 必填                         |
| 诊断时长   | 持续诊断时长（时间单位：s）        | 必填，默认为 15s             |
| 目标磁盘   | 需要检测的磁盘名称，如sda，sdb等。 | 选填（不指定则诊断所有磁盘） |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138825146-ae5e7247-2ab3-44d0-b44b-3ed58d3dbcae.png#clientId=u9cf628e7-b5c5-4&from=paste&height=944&id=ud8785c8b&name=image.png&originHeight=1888&originWidth=3806&originalType=binary&ratio=2&rotation=0&showTitle=false&size=703440&status=done&style=none&taskId=u60e0d68f-8e58-4d83-ab2c-953362e7660&title=&width=1903)
**诊断结论：**这部分展示一个summary信息，如在诊断期间检测到了多大的IO流量（_通过iops、bps表示在result列_），产生这么大的流量的原因是什么（_展示在reason列，一般形式为：某某进程，访问某磁盘，iops是多大，IO吞吐是多大，访问的目标文件是什么；其中针对kworker刷脏这类异步IO，支持对实际写buffer io进程的溯源能力_），以及相关处理建议；
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138868602-f7335677-dd14-433c-83fa-4e29ae2a96d6.png#clientId=u9cf628e7-b5c5-4&from=paste&height=74&id=u4db10619&name=image.png&originHeight=148&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=135486&status=done&style=none&taskId=uc8dcb969-06db-4f6e-9ca0-cf4fd14a852&title=&width=750)
在诊断结果最右侧支持磁盘的checklist选择展示不同磁盘的数据，注意切换的是即将介绍的**磁盘IO流量分析、进程IO流量分析、进程IO块大小分析**这三部分的数据展示：
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138882702-eb477d27-05aa-418a-b2f0-9fe3d2969bae.png#clientId=u9cf628e7-b5c5-4&from=paste&height=35&id=ubf16c8d4&name=image.png&originHeight=69&originWidth=218&originalType=binary&ratio=2&rotation=0&showTitle=false&size=11242&status=done&style=none&taskId=u912b215a-f316-4037-8a7e-ce30f5c5eb8&title=&width=109)
**磁盘 IO 流量分析：** 该部分展示诊断期间，统计到的磁盘IO流量，支持通过磁盘的checklist选择展示不同磁盘的数据，主要字段：

- `diskname`：磁盘/分区名称
- `r_rqm`：诊断期间合并读操作的次数
- `w_rqm`：诊断期间合并写操作的次数
- `r_bps`：诊断期间读IO吞吐
- `w_bps`：诊断期间写IO吞吐
- `wait`：每个I/O平均所需的时间
- `r_wait`：每个读操作平均所需的时间
- `w_wait`：每个写操作平均所需的时间
- `util%`：该硬盘设备的繁忙比率

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677138934013-67f9aebd-fac7-4c97-b184-2e8b22e5ae69.png#clientId=u9cf628e7-b5c5-4&from=paste&height=73&id=u032310db&name=image.png&originHeight=146&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=117258&status=done&style=none&taskId=u91c717a3-12c2-4b1e-b8e0-e87ed24f73c&title=&width=750)
**进程 IO 流量分析：** 该部分展示诊断期间，进程级别IO流量，支持通过磁盘的checklist选择展示不同磁盘的数据，主要字段：

- `comm` : 进程名 
- **tgid:pid** : 进程id信息
- **iops_rd** : 进程贡献的读iops 
- **bps_rd**  : 进程贡献的读bps 
- **iops_wr** : 进程贡献的写iops 
- **bps_wr**  : 进程贡献的写bps
- **flushIO**：表示此进程产生的flush IO个数
- **device**：消费IO的目标磁盘
- **file**：产生IO的来源文件

其中尤其注意到，该功能支持kworker刷脏的IO溯源能力，所以可以看到在kworker进程下面存在子项，展开可以看到实际产生脏页（写buffer io）的进程
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2022/png/326738/1670325261877-41b2c2aa-a8a2-4d3f-9f93-c0868ec24c3a.png#clientId=ue6caa064-d3a4-4&from=paste&height=321&id=uea3c9926&name=image.png&originHeight=321&originWidth=2492&originalType=binary&ratio=1&rotation=0&showTitle=false&size=364051&status=done&style=none&taskId=u08d1dba0-4f74-43af-bdb5-ce9cb2e0869&title=&width=2492)
**进程 IO 块大小分析：** 该部分展示诊断期间，进程级别IO提交到OS块层的块大小分布，支持通过磁盘的checklist选择展示不同磁盘的数据，其中pat_WNK，表示IO大小为nKB以内的IO个数占比，large则表示大于512KB的大块IO的占比。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677139232917-d427d1a8-08f7-4f4c-92b3-4dfd8b612e98.png#clientId=u9cf628e7-b5c5-4&from=paste&height=76&id=u5f99e14e&name=image.png&originHeight=152&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=122755&status=done&style=none&taskId=u6d1a9866-008d-4bc0-9272-4350f25b6ce&title=&width=750)
##### 5. 离线导入
使用方式同调度抖动诊断。
#### IO hang 诊断
##### 1. 使用场景
当存储设备异常时，业务进程的 IO 请求长时间得不到响应导致业务进程 Hang 住。
##### 2. 功能描述
监控系统每个存储设备的IO访问路径上是否存在IO HANG问题。（检测出问题后，可以选择将IO流量切换到正常的存储设备上，隔离异常的存储设备）
##### 3. 诊断参数
| **参数名** | **参数说明**                                   | **是否必填**                 |
| ---------- | ---------------------------------------------- | ---------------------------- |
| 实例IP     | 目标Node节点的IP                               | 必填                         |
| 诊断时长   | 持续诊断时长（时间单位：s）                    | 必填，默认为 10s             |
| 时间阈值   | 保留IO延迟大于设定时间阈值的IO（时间单位：ms） | 必填，默认为 5000ms          |
| 目标磁盘   | 需要检测的磁盘名称，如sda，sdb等。             | 选填（不指定则诊断所有磁盘） |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677139397599-97f1f049-a30d-40e2-8c9b-25713a8e2fe6.png#clientId=u9cf628e7-b5c5-4&from=paste&height=940&id=ud4c445ec&name=image.png&originHeight=1880&originWidth=3806&originalType=binary&ratio=2&rotation=0&showTitle=false&size=630233&status=done&style=none&taskId=u8cf1b013-caba-451c-8f18-0fccfca168c&title=&width=1903)
**IO HANG 总览：** 表示在一个诊断周期内，检测是否有IO HANG，无IO HANG时，check result的状态为normal，字体显示绿色，有IO HANG时，check result的状态为abnormal，字体显示红色，而Number of OS HANG或者Number of Disk HANG，分别表示IO HANG在OS或者磁盘的IO个数。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677139960819-0f82dd81-f072-4294-88e3-96e40c28abf3.png#clientId=u9cf628e7-b5c5-4&from=paste&height=63&id=u11fa23de&name=image.png&originHeight=125&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=95838&status=done&style=none&taskId=u5198ce42-724b-4f41-9860-d184b85dd74&title=&width=750)
在诊断结果最右侧支持磁盘的checklist选择展示不同磁盘的数据，切换数据展示：
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677139975471-0f420d95-1b44-4333-98de-eaef23355cfa.png#clientId=u9cf628e7-b5c5-4&from=paste&height=35&id=ua3921d94&name=image.png&originHeight=69&originWidth=218&originalType=binary&ratio=2&rotation=0&showTitle=false&size=11242&status=done&style=none&taskId=ud3180ae9-f06a-456b-9376-2c1ff0984f0&title=&width=109)
**TOP 10 详情：** 这部分展示了HANG住持续时间的前TOP 10个IO的详情

- `time`：表示检测到此IO HANG的时间日期，精确到ms
- `abnormal`：表示此IO HANG在哪，以“HANG在哪（具体原因）HANG了多久”形式展示
- `iotype`：表示具体的IO操作，如Write/Read/Discard等等
- `sector`：表示此IO要访问磁盘的扇区编号
- `datalen`：表示此IO要访问的数据长度，单位byte
- `iostate`：表示此IO的状态，inflight表示未完成、complete表示已经标记完成
- `cpu`：表示此IO由进程在哪个cpu发起
- `comm`（待扩展）：表示此IO的生产者进程名
- `pid`（待扩展）：表示此IO的生产者进程id
- `file`（待扩展）：表示此IO的文件路径

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677140045571-4654cde9-ef74-40fc-b472-1cc3d9ea935c.png#clientId=u9cf628e7-b5c5-4&from=paste&height=145&id=u8d6792fc&name=image.png&originHeight=290&originWidth=1500&originalType=binary&ratio=2&rotation=0&showTitle=false&size=245135&status=done&style=none&taskId=u98fad527-6641-4160-9205-04bd36ca42e&title=&width=750)
##### 5. 离线导入
使用方式同调度抖动诊断。
### 网络诊断中心
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机（延迟诊断需要两台以上的机器）
- 主机登录权限需要是root权限（执行 sysak 需要）
#### 丢包诊断
##### 1. 使用场景
由于硬件或者网络环境的因素导致大量丢包，造成业务进程响应慢等。
##### 2. 功能描述
记录丢包事件、丢包发生的硬件或网卡设备、丢包点等，并进一步分析丢包的原因，帮助用户诊断定位网络丢包的问题。
##### 3. 诊断参数
| **参数名** | **参数说明**                | **是否必填**     |
| ---------- | --------------------------- | ---------------- |
| 实例IP     | 目标Node节点的IP            | 必填             |
| 运行时间   | 持续诊断时长（时间单位：s） | 必填，默认为 10s |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677141484801-4643a7ce-c329-47a5-b4d4-3401b78387cd.png#clientId=u9cf628e7-b5c5-4&from=paste&height=780&id=uc459331d&name=image.png&originHeight=1560&originWidth=3798&originalType=binary&ratio=2&rotation=0&showTitle=false&size=534838&status=done&style=none&taskId=ucdacd65a-7214-4067-80cf-a6dacc27bf7&title=&width=1899)
丢包诊断以表格方式呈现，具体解析如下：

1. 时间戳：显示时间信息；
2. 网卡名称：如果是硬件或者网卡相关丢包，则会显示网卡名称。
3. 丢包点：丢包点的形式是： 内核丢包函数名+丢包点偏移+丢包次数。
4. 丢包计数：主要展示当前周期内snmp、dev、netstat等相关丢包指标的变化情况。
5. 丢包原因：显示丢包原因。
##### 5. 离线导入
使用方式同调度抖动诊断。
#### 抖动诊断
##### 1. 使用场景
由于网络收发路径的某段延迟增大导致业务进程的网络请求发生抖动。
##### 2. 功能描述
基于 ICMP 报文，测出 ping 发起端的报文时延（发送路径）以及 ping 接收端的报文时延（接收路径）。
##### 3. 诊断参数
| **参数名** | **参数说明**                | **是否必填**     |
| ---------- | --------------------------- | ---------------- |
| 实例IP     | 目标Node节点的IP            | 必填             |
| 运行时间   | 持续诊断时长（时间单位：s） | 必填，默认为 10s |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677143053862-c1061938-c56d-4e30-8fb1-550c52df1b5a.png#clientId=u9cf628e7-b5c5-4&from=paste&height=942&id=u3952bbc2&name=image.png&originHeight=1884&originWidth=3806&originalType=binary&ratio=2&rotation=0&showTitle=false&size=594860&status=done&style=none&taskId=u0c0748e2-9792-4d3e-a3a6-022dcd42ad7&title=&width=1903)
##### 5. 离线导入
使用方式同调度抖动诊断。
#### 重传诊断
##### 1. 使用场景
由于网络拥塞等原因导致业务进程的 TCP 连接发生大量重传。
##### 2. 功能描述
记录重传的时间、IP、TCP socket所处的状态和拥塞情况，帮助用户了解网络重传发生的情况。
##### 3. 诊断参数
| **参数名** | **参数说明**                | **是否必填**     |
| ---------- | --------------------------- | ---------------- |
| 实例IP     | 目标Node节点的IP            | 必填             |
| 运行时间   | 持续诊断时长（时间单位：s） | 必填，默认为 10s |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677143413688-f3392633-bac3-4f26-a507-ef65ae80dcf1.png#clientId=u9cf628e7-b5c5-4&from=paste&height=950&id=udc3208e3&name=image.png&originHeight=1900&originWidth=3798&originalType=binary&ratio=2&rotation=0&showTitle=false&size=610553&status=done&style=none&taskId=u1eaa3e9b-4795-409e-946e-9846ecda0a2&title=&width=1899)
重传诊断以表格方式呈现，具体解析如下：

1. 时间戳：显示重传时间；
2. ip地址：五元组信息；
3. tcp状态：tcp sock所处的状态；
4. 拥塞状态：拥塞机制所处的状态；
##### 5. 离线导入
使用方式同调度抖动诊断。
#### 时延诊断
##### 1. 使用场景
用于定位网络延迟大是通信链路的那一阶段导致的
##### 2. 功能描述
抖动诊断目前仅支持icmp报文，即ping。其包含两个部分，一个是ping发起端的报文时延，即发送报文路径，另外一个是ping接收端的报文时延，即接收报文路径。
##### 3. 诊断参数
| **参数名**                 | **参数说明**                | **是否必填**     |
| -------------------------- | --------------------------- | ---------------- |
| 源实例IP                   | 源节点IP                    | 必填             |
| 目标实例IP                 | 目的节点IP                  | 必填             |
| 运行时间                   | 持续诊断时长（时间单位：s） | 必填，默认为 10s |
| 追踪包数                   | 模拟发包的最大报文数        | 必填，默认为 100 |
| 间隔毫秒数                 | 模拟发包时两次发包的        |
| 间隔毫秒数（时间单位：ms） | 必填，默认为 1000ms         |
| 报文协议                   | 模拟发包的报文协议          | 必填，默认为ICMP |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144163112-8ecdeab8-bfc6-4d2c-8db1-a0b751dffcf6.png#clientId=u9cf628e7-b5c5-4&from=paste&height=942&id=ud96a1bc1&name=image.png&originHeight=1884&originWidth=3822&originalType=binary&ratio=2&rotation=0&showTitle=false&size=684705&status=done&style=none&taskId=u482d03c1-2a6b-4079-9e53-43529ac6acc&title=&width=1911)
**发送报文路径：** 发送报文路径是 ping 的发起端的 icmp 报文时延信息，包含：

1. 内核发送：表示 icmp echo 报文在内核发送路径的时延；
2. 外部链路：表示 icmp 报文在外部链路的耗时；
3. 内核接收：表示 icmp echo reply报文内核接收路径的时延。

**接收报文路径：** 接收报文路径是ping的接收端的 icmp 报文时延信息，包含：

1. 内核接受：表示 icmp echo 报文在内核接收路径的时延；
2. 内核发送：表示 icmp echo reply 报文内核发送路径的时延。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144458611-1389d524-e96a-4d14-b9b6-227c495fdd18.png#clientId=u9cf628e7-b5c5-4&from=paste&height=428&id=uf9ebc31e&name=image.png&originHeight=856&originWidth=3748&originalType=binary&ratio=2&rotation=0&showTitle=false&size=230081&status=done&style=none&taskId=u3f226fa7-f0ea-49f4-8a0c-733b1238fa6&title=&width=1874)
##### 5. 离线导入
使用方式同调度抖动诊断。
### 内存诊断中心
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机
- 主机登录权限需要是root权限（执行 sysak 需要）
#### 内存大盘
##### 1. 使用场景
内存相关问题
##### 2. 功能描述
对内存的整体分布和组成进行分析和展示。
##### 3. 诊断参数
| **参数名** | **参数说明**     | **是否必填** |
| ---------- | ---------------- | ------------ |
| 实例IP     | 目标Node节点的IP | 必填         |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144625942-87cca12d-46c0-40ed-b553-1e710e792902.png#clientId=u9cf628e7-b5c5-4&from=paste&height=939&id=uc6526bbd&name=image.png&originHeight=1878&originWidth=3802&originalType=binary&ratio=2&rotation=0&showTitle=false&size=607548&status=done&style=none&taskId=u28539274-ea8c-486a-b99c-6442cf52874&title=&width=1901)
**内存事件：** 主要对内存相关风险一键检查，快速识别系统是否存在内存问题

- 内存利用率：系统内存利用率；
- 内存泄漏：排查系统是否存在泄漏，以及泄漏的类型，目前支持slab, vmalloc和伙伴系统三种泄漏识别；
- memcg残留：评估memory cgroup是否存在残留，过多的残留会严重影响系统性能，以及造成统计数据异常；
- 内存碎片化：评估系统是否存在内存碎片化，内存碎片化会导致系统性能下降

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144853316-15bbf7a0-6cd0-4913-8a6a-3ae832194408.png#clientId=u9cf628e7-b5c5-4&from=paste&height=177&id=K698q&name=image.png&originHeight=354&originWidth=3768&originalType=binary&ratio=2&rotation=0&showTitle=false&size=73698&status=done&style=none&taskId=uac94aab5-f02b-46b2-be8d-128305980a8&title=&width=1884)
**内存总览（Overview）：** 内存总览展示系统所有内存的使用情况，实现内存的可维可测可追踪(1+1+1=3)，也就是总内存 = 内核内存（kernel） + 应用内存(app) + 空闲内存(free)，然后再进一步细分内核内存和应用内存。

- **总内存**
   - `kernel`：表示操作系统内核内存总使用量
   - `app`：表示用户态程序内存总使用量
   - `free`：系统空闲内存
- **内核内存：** 内核态内存，我们重点关注Sunreclaim，伙伴系统的使用量，我们通常说的内存泄漏通常都是这两个指标异常。
   - `Sreclaimable`：Slab可回收内存
   - `Sunreclaim`：Slab不可回收内存
   - `PageTables`：内核页表占用内存
   - `Vmalloc`：内核调用vmalloc分配的内存
   - `KernelStack`：进程的内核堆栈总内存
   - `AllocPages`：内核调用alloc_pages等接口直接从伙伴系统分配的内存量，这类内存无法通过任何节点文件获取，使用过多会造成内存黑洞。
- **应用内存：** 用户态内存，我们重点关注anon，shmem，filecache
   - `filecache`：文件缓存内存，这类内存可以通过drop caches回收。
   - `anon`：匿名内存，程序堆和栈内存，匿名内存占用过多，需要评估业务进程是否有内存泄漏，系统是否开启了透明大页。
   - `mlock`：系统锁住的内存量
   - `huge`：hugetlb大页内存量
   - `buffer`：块设备以及文件系统元信息占用的内存量
   - `shmem`：共享内存（tmpfs）。业务进程退出后，经常会忘记删除tmpfs文件，或者在打开状态，直接删掉tmpfs文件，都会操作shmem泄漏。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144941122-887f04de-3c78-4799-b945-82c38fb84170.png#clientId=u9cf628e7-b5c5-4&from=paste&height=318&id=u42554d07&name=image.png&originHeight=636&originWidth=3776&originalType=binary&ratio=2&rotation=0&showTitle=false&size=313175&status=done&style=none&taskId=u8a61d346-a44d-404d-b185-f858f516ca6&title=&width=1888)
**进程内存：** 以进程维度按内存大小排序，并分解成匿名内存，文件缓存，共享内存。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677144996803-3c621680-3d6a-4b0b-8225-fb83d8f1687a.png#clientId=u9cf628e7-b5c5-4&from=paste&height=661&id=ud89601a9&name=image.png&originHeight=1322&originWidth=3728&originalType=binary&ratio=2&rotation=0&showTitle=false&size=362567&status=done&style=none&taskId=u9c251029-5c81-49df-adaf-56d901c80bd&title=&width=1864)
**Cache Top：** 展示 Cache 占用最多前五个文件和对应的进程。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677145200611-ee2ae307-71c5-4bfe-b860-1e5eaa110014.png#clientId=u9cf628e7-b5c5-4&from=paste&height=479&id=uc298d0f7&name=image.png&originHeight=958&originWidth=3752&originalType=binary&ratio=2&rotation=0&showTitle=false&size=442985&status=done&style=none&taskId=u0ac75179-64d0-4550-a05e-b27847e440b&title=&width=1876)
##### 5. 离线导入
使用方式同调度抖动诊断。
#### Cache 分析
##### 1. 使用场景
内存不足且Cache占用过高。
##### 2. 功能描述
分析容器，容器组，整机，cgroup的cache内存由哪些文件引入，以及每个文件引入的active cache和inactive cache。
##### 3. 诊断参数
| **参数名** | **参数说明**     | **是否必填**           |
| ---------- | ---------------- | ---------------------- |
| 实例IP     | 目标Node节点的IP | 必填                   |
| 容器       | 容器ID或者pod名  | 必填，默认填空字符串"" |
| 诊断类型   | 诊断类型：       |
all: 诊断实例上所用容器cache
host: 诊断实例主机cache
pod:诊断pod内部cache
container: 诊断容器内部cache | 必填，默认填"all" |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677145357171-f160858c-b3a5-4aaa-9800-d41a17a903db.png#clientId=u9cf628e7-b5c5-4&from=paste&height=858&id=uc920c01c&name=image.png&originHeight=1716&originWidth=3778&originalType=binary&ratio=2&rotation=0&showTitle=false&size=672942&status=done&style=none&taskId=ue66ad097-79ec-458c-93e4-e3e2fdd6b4c&title=&width=1889)
**缓存排序：** 对缓存占用最多的几个文件的组成进行分析展示
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677145467075-cf6a3759-9ab3-4153-bbf0-20000d6aa6c3.png#clientId=u9cf628e7-b5c5-4&from=paste&height=722&id=u4c34e39e&name=image.png&originHeight=1444&originWidth=3750&originalType=binary&ratio=2&rotation=0&showTitle=false&size=815748&status=done&style=none&taskId=u381854d4-5e9e-434d-8f65-c658c11b1c1&title=&width=1875)
##### 5. 离线导入
使用方式同调度抖动诊断。
#### OOM诊断
##### 1. 使用场景
业务进程内存泄露容易导致系统发送 OOM，当OOM发生时伴随着大量内核日志，而这些内核日志往往难于分析。
##### 2. 功能描述
本项诊断功能可以帮助用户定位定位内存泄漏的位置，并分析导致内存泄露的原因。
##### 3. 诊断参数
| **参数名** | **参数说明**                                                               | **是否必填** |
| ---------- | -------------------------------------------------------------------------- | ------------ |
| 实例IP     | 目标Node节点的IP                                                           | 必填         |
| 诊断时间   | 指定检测某个过去的时间，不指定代表检查最近一次，填写格式为：unix毫秒时间戳 | 选填         |

##### 4. 在线诊断
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677145599442-a8dffcad-dea6-4a02-88e6-a56352c60e45.png#clientId=u9cf628e7-b5c5-4&from=paste&height=943&id=ub2028e23&name=image.png&originHeight=1886&originWidth=3814&originalType=binary&ratio=2&rotation=0&showTitle=false&size=591617&status=done&style=none&taskId=u9dcfc111-3f5d-498c-9d07-6f20e1a567e&title=&width=1907)
##### 5. 离线导入
使用方式同调度抖动诊断。
### 自定义诊断中心
#### 前提条件

- 在主机管理界面已经成功添加一台或多台主机
#### 操作步骤
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/67256280/1677146009597-72ad8788-7b73-45b9-baa6-dde6fa4272c8.png#clientId=u9cf628e7-b5c5-4&from=paste&height=947&id=uda700072&name=image.png&originHeight=1894&originWidth=3834&originalType=binary&ratio=2&rotation=0&showTitle=false&size=579652&status=done&style=none&taskId=u9debd36c-1d4e-4aa7-ab3a-b119121757d&title=&width=1917)
## 日志中心
### 概述
日志中心将会为用户提供日志审计的功能，便于管理员监控、管理所负责集群的运维情况。
### 审计日志
#### 前提条件
无
#### 操作步骤

1. 进入“日志中心”菜单，下拉菜单中选择点击”审计日志“进入“审计日志”页面。
2. 在“审计日志”界面中，会展示所有接口调用的情况。日志列表简要说明：

| 列表名   | 说明                               |
| -------- | ---------------------------------- |
| 时间     | API接口访问时间                    |
| 用户     | API接口调用的用户                  |
| 源IP     | 用户源IP                           |
| 请求URL  | 接口API                            |
| 请求方法 | 接口API的请求方法，GET/POST等      |
| 返回状态 | 接口返回码，用于判断成功或者失败   |
| 行为类型 | 接口的行为，操作行为，登陆行为等。 |

### 任务日志
#### 前提条件
无
#### 操作步骤

1. 进入“日志中心”菜单，下拉菜单中选择点击”任务日志“进入“任务日志”页面。
2. 在“任务日志”界面中，会展示诊断任务的情况。日志列表简要说明：

| 列表名   | 说明                       |
| -------- | -------------------------- |
| 时间     | API接口访问时间            |
| 任务ID   | 诊断中心中发起诊断的ID     |
| 状态     | 诊断任务成功或者失败       |
| 参数列表 | 发起诊断任务请求的入参列表 |

### 告警日志
#### 前提条件
无
#### 操作步骤

1. 进入“日志中心”菜单，下拉菜单中选择点击”告警日志“进入“告警日志”页面。
2. 在“告警日志”界面中，会展示所有告警消息的情况。日志列表简要说明：

| 列表名   | 说明                             |
| -------- | -------------------------------- |
| 告警时间 | 告警时间                         |
| 告警级别 | 告警级别，成功success，失败error |
| 类型     | 告警类型，目前是notification     |
| 是否已读 | 告警是否已读                     |
| 告警内容 | 告警具体内容                     |

## 安全中心
### 概述
安全问题越来越受到大家的重视，而操作系统包含大量的开源软件包，无时无刻不在产生新的安全漏洞。作为操作系统管理员最关心的是能够及时发现系统中存在的安全漏洞，并能够快速修复。SysOM安全中心为用户提供实时的安全漏洞检测，同时根据操作系统的修复进展提供白屏修复操作。
### 漏洞中心
#### 前提条件
已成功添加一台或多台主机，主机登陆权限需要是root权限。
新加的主机本身可以连接repo源。执行dnf updateinfo list能返回CVE信息。
#### 操作步骤

1. 进入“安全中心”菜单，下拉菜单中选择点击”漏洞中心“进入“漏洞中心”页面。
2. 点击“漏洞库配置”查看，SysOM默认添加的是龙蜥操作系统的漏洞库。因此默认是支持龙蜥操作系统的漏洞扫描，如果需要支持其它操作系统，需要“新建漏洞数据库”。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676965299823-2e0cdc22-0711-401c-947f-0bf370ef06d9.png#clientId=u874c5459-c5a2-4&from=paste&height=241&id=u645e0599&name=image.png&originHeight=482&originWidth=2780&originalType=binary&ratio=2&rotation=0&showTitle=false&size=640315&status=done&style=none&taskId=u020ea753-eaa4-402c-917a-4c4333ac569&title=&width=1390)

3. 回退到“漏洞中心”界面，点击“一键扫描”，即可对所有主机列表的主机进行漏洞扫描。

抬头总结的信息会有本次一键扫描的总结性展示，
需要修复的漏洞（CVE）：所有主机涉及的CVE总数
需要修复的高危漏洞（CVE）：CVE列表中高危级别的CVE个数
存在漏洞的主机：涉及漏洞的主机数
今日已修复漏洞：今日已修复漏洞的个数
累计已修复漏洞：累计已修复漏洞的个数
下面漏洞列表的说明如下：

| **参数** | **数值**    | **说明**                               |
| -------- | ----------- | -------------------------------------- |
| 序号     | 1，2，3     | 表格排序序号                           |
| 编号     | CVE-年份-ID | CVE漏洞的编号                          |
| 发布时间 | 年-月-日    | CVE漏洞的发布时间                      |
| 漏洞等级 | 高中低危    | CVE漏洞的漏洞等级                      |
| 涉及主机 | 主机列表    | 本次扫描中相关CVE漏洞涉及的主机列表    |
| 操作     | 修复        | 超链接，可点击，跳转到修复界面直接修复 |

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676961888147-52b8f6de-1d2d-4a33-9ff1-0f49550ded69.png#clientId=u874c5459-c5a2-4&from=paste&height=629&id=eqnxV&name=image.png&originHeight=1258&originWidth=2816&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1764909&status=done&style=none&taskId=u32ddd17e-24f2-421b-aeeb-6d944250ba1&title=&width=1408)

4. 在漏洞列表中，最后一列，选择某一个漏洞，点击“修复”，跳转到修复界面：

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676966507568-7f6b63d7-cd13-4a9e-8fba-1e6643f60d6c.png#clientId=ub79c1966-f5bf-4&from=paste&height=555&id=u02ccbbed&name=image.png&originHeight=1110&originWidth=2786&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1420568&status=done&style=none&taskId=ue3d12f90-1e52-4586-b6c4-d7b68ae4253&title=&width=1393)

5. 可以选择对某一台主机，点击右侧“修复”按钮进行修复，也可以批量选择多台主机，然后点击右下角的“一键修复”按钮进行修复。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676969439912-588ff5a0-2022-4716-8eb0-cfe10fc13e8c.png#clientId=ub09ad29e-e8fb-4&from=paste&height=509&id=ude4f4b01&name=image.png&originHeight=1018&originWidth=2728&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1366545&status=done&style=none&taskId=uc39c4b3a-c8be-4b18-b1b5-37ca49c591a&title=&width=1364)

6. 修复完成后，会弹出“系统漏洞已修复”的提示框。点击“OK”，会跳转到“漏洞中心”页面。
7. 在“漏洞中心”界面，点击“历史修复”按钮，可以查看“历史修复漏洞信息”

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676970031021-0e9d49ad-5e36-412d-80fc-648a243ce35c.png#clientId=ub09ad29e-e8fb-4&from=paste&height=289&id=uad8e579a&name=image.png&originHeight=1070&originWidth=2776&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1576705&status=done&style=none&taskId=u894c119b-041e-4ed3-9d9c-e075874ff05&title=&width=749)

8. 在“历史修复漏洞信息”界面，选择列表某一行，点击“查看详情”，可以查看该CVE的修复情况。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676970352874-1966b687-ab1a-4ed4-8167-d1720c31b1a6.png#clientId=ub09ad29e-e8fb-4&from=paste&height=360&id=u36ea38ca&name=image.png&originHeight=720&originWidth=2780&originalType=binary&ratio=2&rotation=0&showTitle=false&size=977853&status=done&style=none&taskId=u003873c5-23c6-49a7-8c94-91ebaa47589&title=&width=1390)

9. 继续点击“CVE修复详情”，可以查看该次修复的状态。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/260640/1676970310796-d49eed64-dec0-4168-a99c-2d92d5c125cd.png#clientId=ub09ad29e-e8fb-4&from=paste&height=187&id=ua1fe5b31&name=image.png&originHeight=374&originWidth=2792&originalType=binary&ratio=2&rotation=0&showTitle=false&size=411741&status=done&style=none&taskId=u1a1d2708-b728-43aa-912a-56402eaa866&title=&width=1396)


## 热补丁中心
### 概述
内核热补丁，也叫hotfix。是一种以模块化的方式给linux内核打补丁而无需重启系统的一项技术。基于这种技术，管理员可以在不重启机器的情况下，修复内核的BUG或者是安全漏洞，以此来提升系统的可用性以及稳定性。Sysom热补丁中心提供便捷的界面设计，允许用户通过简洁明了的方式制作热补丁，降低热补丁制作的门槛，为用户打造方便使用的热补丁制作和管理平台。
### 部署
热补丁中心存在单机部署以及多机部署的差异。其重要差异在于：单机部署场景下，hotfix的制作在sysom服务器上，一台机器既充当服务器、也充当builder，这对机器的性能有较高的要求；多机部署场景下，hotfix的制作可以在单独设置为builder的机器上，对服务器的压力要小很多。用户可以按照其自身场景使用这两种部署方式。
#### 热补丁中心的关键服务
热补丁中心有两个关键服务：1、sysom-hotfix；2、sysom-hotfix-builder。其配置在sysom/script/conf文件中的描述为：
```
[hotfix]
6_sysom_hotfix

[hotfix-builder]
6_sysom_hotfix_builder
```
在conf文件中填写该服务即开启该服务，如果需要关闭某项服务，将其从conf文件中删除即可。
#### 部署方式
1、单机部署
单机部署场景下，只需要默认部署运行sysom即可。sysom默认会开启sysom_hotfix_builder服务。确保conf文件中同时填写了[hotfix]与[hotfix-builder]两项服务。
sysom默认在init.sh程序中的nfs目录指向本机。
```
#! /bin/bash
SERVER_DIR="sysom_server"
HOTFIX_BUILDER_DIR=${SERVER_DIR}/sysom_hotfix_builder
VIRTUALENV_HOME=${SERVER_HOME}/virtualenv
SERVICE_NAME=sysom-hotfix-builder
NFS_SERVER_IP=${SERVER_LOCAL_IP}
```
2、多机部署
多级部署要求builder与服务器在同一网段内（需要nfs链接）。如果需要构建与本机架构不同的热补丁，则必须使用多机部署。
在sysom服务器中，必须开启[hotfix]服务（建议关闭[hotfix-builder]），在builder机器中，则需要开启[base]和[hotfix-builder]，下面为一个builder的示例：
```
[base]
0_env
0_local_services
0_sysom_api
0_sysom_channel

[hotfix-builder]
6_sysom_hotfix_builder
```
在采用多机部署的场景下，需要对以下文件进行修改：
1、/sysom/script/server/6_sysom_hotfix_builder/init.sh
```
#! /bin/bash
SERVER_DIR="sysom_server"
HOTFIX_BUILDER_DIR=${SERVER_DIR}/sysom_hotfix_builder
VIRTUALENV_HOME=${SERVER_HOME}/virtualenv
SERVICE_NAME=sysom-hotfix-builder
NFS_SERVER_IP=110.110.110.110  # 将这里的SERVER_LOCAL_IP改为sysom服务器的私网ip地址
															 # 进行与sysom服务器的nfs文件共享
```
2、/sysom/sysom_server/sysom_hotfix_builder/builder.ini
```
[sysom_server]
server_ip = http://127.0.0.1     # 指向sysom服务器的ip
account = account								 # 登录sysom服务器的帐号
password = password							 # 登录sysom服务器的密码

[cec]
cec_url = redis://127.0.0.1:6379 # 指向sysom服务器的redis地址，填入sysom服务器的私网ip

[builder]
hotfix_base = /hotfix_build/hotfix  # 可以默认，hotfix构建的工作目录
nfs_dir_home = /usr/local/sysom/server/builder/hotfix # 可以默认，builder下用于nfs共享的目录路径
package_repo = /hotfix/packages     # 可以默认，用于存储下载缓存文件的文件夹
```
修改完成后，在完成sysom的服务器部署后，在builder上部署以上配置的sysom即可。
### 热补丁列表
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678698352740-61ed3f94-8f5c-4af3-bd40-901ba292480e.png#clientId=ued280db6-8b06-4&from=paste&height=650&id=ub1f6e5d5&name=image.png&originHeight=1300&originWidth=3830&originalType=binary&ratio=2&rotation=0&showTitle=false&size=929218&status=done&style=none&taskId=u8f275a9b-5b21-47f8-9616-2012c459f08&title=&width=1915)
在该页面下，输入栏一共有四个，分别是：创建时间、内核版本、热补丁名称以及patch文件名称。以上四个输入栏输入的结果用于筛选转正式包的热补丁。输入你希望筛选的条件，点击查询按钮，就会利用上述输入的条件进行筛选并显示在下方的热补丁列表中。
### 热补丁制作
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678775657368-8457d606-b41c-43fd-b8e8-5c9a604aa994.png#clientId=u60f86884-44b1-4&from=paste&height=583&id=uc656b192&name=image.png&originHeight=1166&originWidth=3832&originalType=binary&ratio=2&rotation=0&showTitle=false&size=928065&status=done&style=none&taskId=u2057c141-739a-4930-82d0-c132031e878&title=&width=1916)
在热补丁制作页面中，在输入栏中一共有三个输入栏，分别是内核版本、热补丁名称、文件上传。在该页面下，可以完成某个内核版本的热补丁制作。
#### 热补丁制作
Sysom的热补丁中心默认支持Anolis的版本。Anolis的hotfix制作直接输入Anolis内核版本号以及所需参数即可制作。
1、输入内核版本、热补丁名称以及点击文件上传按钮，点击文件上传按钮以后，会弹出一下界面
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678775876979-5c65d512-aefe-4c02-bed9-9cbd2605b367.png#clientId=u60f86884-44b1-4&from=paste&height=287&id=u12372438&name=image.png&originHeight=1210&originWidth=1966&originalType=binary&ratio=2&rotation=0&showTitle=false&size=737264&status=done&style=none&taskId=u72284e34-ee74-44a1-868d-277c1d7c0a1&title=&width=466)

选择需要用于制作热补丁的patch。如果文件成功上传至服务器，则会显示如下图所示的情况。如果失败，下方的补丁将显示为红色字体。

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678775925273-b2a97309-6f94-4949-a346-ead65d216aef.png#clientId=u60f86884-44b1-4&from=paste&height=118&id=uf594cb51&name=image.png&originHeight=236&originWidth=680&originalType=binary&ratio=2&rotation=0&showTitle=false&size=43629&status=done&style=none&taskId=u2cfaf405-a76b-4b64-8a0d-01ef90035cb&title=&width=340)

点击制作。在下方热补丁列表中会显示所提交的任务信息。如下图所示：

![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678776006724-7d3a5213-53fd-47b0-bf77-e808430935c5.png#clientId=u60f86884-44b1-4&from=paste&height=307&id=ua63aed2f&name=image.png&originHeight=614&originWidth=3800&originalType=binary&ratio=2&rotation=0&showTitle=false&size=499954&status=done&style=none&taskId=u547727ff-711d-4461-bf1a-728f3eb6fc8&title=&width=1900)

热补丁任务一共有四个状态：等待构建、正在构建、构建成功、构建失败。如果发现热补丁任务长期处于等待构建状态，请前往builder机器通过命令查看builder状态。
```
ps -aux | grep builder
```
#### 日志查看
点击查看日志按钮，会跳转到新的页面，该页面下将显示该热补丁任务执行所输出的日志信息。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678776263316-337cb81a-82a4-448c-ae5b-4b744a2800ae.png#clientId=u60f86884-44b1-4&from=paste&height=307&id=u7fbc99a2&name=image.png&originHeight=614&originWidth=3780&originalType=binary&ratio=2&rotation=0&showTitle=false&size=586064&status=done&style=none&taskId=u42b6bb4e-a9d1-4145-9cff-d4c07ea8fe0&title=&width=1890)
#### 转正式包及下载
对于制作成功的热补丁任务，其加载列中的下载按钮将会开放。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678776562328-7522bdd3-d179-4712-975f-dc90b4b41189.png#clientId=u60f86884-44b1-4&from=paste&height=283&id=uf3468fc3&name=image.png&originHeight=566&originWidth=3772&originalType=binary&ratio=2&rotation=0&showTitle=false&size=473090&status=done&style=none&taskId=u5db5cc76-8f47-42da-9132-2fa7f6e20b3&title=&width=1886)
点击下载按钮可以将打包完成后的热补丁下载到您的机器上。而成功后制作的热补丁也可进行转正式包，如下图所示，在转正式包的时候需要您确认转正式包，因为转正后的热补丁是无法撤销其正式包状态的。
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678776535716-5f5e0786-efca-45c8-9071-9cee0bf2caf7.png#clientId=u60f86884-44b1-4&from=paste&height=291&id=u6ca92148&name=image.png&originHeight=582&originWidth=3778&originalType=binary&ratio=2&rotation=0&showTitle=false&size=504032&status=done&style=none&taskId=ueb2c7634-a78b-44e8-b80d-4eb9afb1d68&title=&width=1889)
转正式包以后，该热补丁将可以在热补丁列表中查看到。
### 自定义内核版本配置
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678781470062-271e802f-92f0-4caf-afee-42c5f3f82c8d.png#clientId=ufd5c90a9-0c80-4&from=paste&height=944&id=ua85daac0&name=image.png&originHeight=1888&originWidth=3828&originalType=binary&ratio=2&rotation=0&showTitle=false&size=1467463&status=done&style=none&taskId=ucb625db7-962b-4d31-a8ad-d9da30c7591&title=&width=1914)
更多的用户可能会在其场景下维护他们自己的内核版本。因此，Sysom热补丁中心提供一个配置自定义内核版本的功能，以支持用户使用Sysom热补丁平台来制作他们自己维护的内核版本的热补丁。
在使用自定义内核版本的时候需要进行两部分的配置：1、操作系统配置；2、自定义内核版本配置。
#### 操作系统配置
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678781656966-e701a48c-17c8-41e4-9376-b23f2d781fbc.png#clientId=ufd5c90a9-0c80-4&from=paste&height=328&id=u4daad65d&name=image.png&originHeight=656&originWidth=3764&originalType=binary&ratio=2&rotation=0&showTitle=false&size=496177&status=done&style=none&taskId=u3d9608af-3800-46e7-a0be-62ad4149b7a&title=&width=1882)
在配置操作系统的时候，需要为您的操作系统进行命名，然后配置操作系统的git仓库，以及填入用于构建hotfix的镜像（如果不填入镜像，则会使用Anolis默认提供的镜像进行hotfix构建，但是由于编译环境的差异，可能无法构建hotfix）。
```
操作系统配置解析：
每一个内核版本都会属于某种操作系统的。例如：3.10.0-123.el7.x86_64 -> Centos7
4.19.91-26.an8.x86_64 -> Anolis
所以，需要配置一个操作系统来系列管理具体的内核版本
```
#### 自定义内核版本配置
![image.png](https://intranetproxy.alipay.com/skylark/lark/0/2023/png/23256834/1678782172972-6fc8cf72-9d6a-4f40-9a45-4f6c02fbd07f.png#clientId=ufd5c90a9-0c80-4&from=paste&height=441&id=u1f6429f7&name=image.png&originHeight=882&originWidth=3790&originalType=binary&ratio=2&rotation=0&showTitle=false&size=681990&status=done&style=none&taskId=u4e30a2e6-4db0-441c-ba74-0c1a80a91fa&title=&width=1895)
在配置自定义内核版本的时候，需要先完成对操作系统的配置。完成操作系统的配置以后，可以在输入栏的操作系统名中的下拉框选择你已经完成配置的操作系统。然后依次填入该内核版本的全量内核版本号（包括架构）、在git仓库下该内核版本的分支或者tag、该内核版本的kernel-devel包的下载地址以及该内核版本的kernel-debuginfo包的下载地址。点击添加，在下方列表中将会显示该内核版本的信息。
#### 自定义版本内核的hotfix制作
在制作自定义内核版本的hotfix时，需要先完成该内核版本的配置，随后直接在《热补丁制作》页面填入已经完成配置的自定义内核即可制作。

